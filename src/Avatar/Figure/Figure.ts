import {OffsetType} from '../Type/OffsetType';
import {OffsetMap} from '../Type/OffsetMap';
import {ParsedLook} from '../Type/ParsedLook';
import {parseStringPromise} from 'xml2js';
import {injectable} from 'tsyringe';

@injectable()
/**
 * @class Figure
 */
export class Figure {
    /**
     * Creates a snapshot of the offset
     *
     * @param figureMapStr FigureMap String
     * @param getManifest Manifest
     * @returns {Promise<string>}
     */
    public async createOffsetSnapshot(
        figureMapStr: string,
        getManifest: (fileName: string) => Promise<string>
    ): Promise<string> {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        const figureMapXml = await parseStringPromise(figureMapStr);
        const figureMap = this.parseFigureMap(figureMapXml);

        const { offsetMap } = await this.loadOffsetMap(figureMap.libraries, getManifest);

        const obj: any = {};

        offsetMap.forEach((value, key) => (obj[key] = value));

        const result = JSON.stringify(obj);

        return result;
    }

    /**
     * Parses the Look of the Avatar
     *
     * @param look Look
     * @returns {ParsedLook}
     */
    public parseLookString(look: string): ParsedLook {
        return new Map(
          look.split('.').map(str => {
            const partData = str.split('-');

            return [
              partData[0],
              {
                setId: Number(partData[1]),
                colorId: Number(partData[2])
              }
            ] as const;
          })
        );
    }

    /**
     * Loads the Offset Map
     *
     * @param figureMap FigureMap
     * @param getManifest Manifest
     * @returns {Promise<{getOffset: OffsetType;offsetMap: Map<string, { offsetX: number; offsetY: number }>;}>}
     */
    private async loadOffsetMap(
        figureMap: string[],
        getManifest: (fileName: string) => Promise<string>
    ): Promise<{
        getOffset: OffsetType;
        offsetMap: Map<string, { offsetX: number; offsetY: number }>;
    }> {
        const offsetMap: OffsetMap = new Map();
        // eslint-disable-next-line @typescript-eslint/prefer-for-of
        for (let i = 0; i < figureMap.length; i++) {
            const fileName = figureMap[i];

            try {
                const manifestStr = await getManifest(fileName);
                const manifestXml = await parseStringPromise(manifestStr);

                const offsets = this.parseOffsetFromManifest(manifestXml);
                offsets.forEach((offset, key) => offsetMap.set(key, offset));
            } catch (e) {
                continue;
            }
        }

        return {
            getOffset: (name) => offsetMap.get(name),
            offsetMap,
        };
    }

    /**
     * Parses the offset of the XML
     *
     * @param manifestXml XML
     * @returns {Map<string, { offsetX: number; offsetY: number }}
     */
    private parseOffsetFromManifest(
        manifestXml: any
        ): Map<string, { offsetX: number; offsetY: number }> {
        const offsetMap = new Map<string, { offsetX: number; offsetY: number }>();
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        manifestXml.manifest.library[0].assets[0].asset.forEach((asset: any) => {
          const offsetStr = asset.param[0].$.value as string;
          const offsets = offsetStr.split(',').map(Number);

          offsetMap.set(asset.$.name, {
            offsetX: offsets[0],
            offsetY: offsets[1]
          });
        });

        return offsetMap;
    }

    /**
     * Parses the FigureMap
     *
     * @param figureMapXml FigureMap
     * @returns {any}
     */
    private parseFigureMap(figureMapXml: any): any {
        const libs: any[] = figureMapXml.map.lib;

        const partToLibrary = new Map<string, string>();
        const libraries: string[] = [];

        libs.forEach((lib: any) => {
            const parts: any[] = lib.part;

            parts.forEach((part) => {
                const uniqueId = this.getUniqueId(part.$.id, part.$.type);
                partToLibrary.set(uniqueId, lib.$.id);
            });

            libraries.push(lib.$.id);
        });

        return {
            getLibraryOfPart: (id: string, type: string) =>
              partToLibrary.get(this.getUniqueId(id, type)),
            libraries,
        };
    }

    /**
     * Gets the unique id
     *
     * @param id ID
     * @param type Type
     * @returns {string}
     */
    private getUniqueId(id: string, type: string): string {
        return `${id}_${type}`;
    }
}