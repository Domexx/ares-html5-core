import {Point} from 'pixi.js';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {injectable} from 'tsyringe';
import {Game} from '../Game';
import {Moveable} from '../Interface/Moveable';
import {RoomPosition} from '../Room/Interface/RoomPosition';
import {RoomObject} from '../Room/RoomObject';
import {Action} from './Action/Action';
import {AvatarAction} from './Enum/AvatarAction';
import {AvatarOptions} from './Interface/AvatarOptions';
import {LookOptions} from './Interface/LookOptions';
import {AvatarSprite} from './Sprite/AvatarSprite';
import {ObjectAnimation} from '../Animation/ObjectAnimation';

@injectable()
/**
 * @class Avatar
 * @extends RoomObject
 * @implements {Moveable}
 */
export class Avatar extends RoomObject implements Moveable {
    /**
     * @private
     * @property
     */
    private avatarSprite: AvatarSprite;

    private moveAnimation:
    | ObjectAnimation<
        | { type: "walk"; direction?: number; headDirection?: number }
        | { type: "move" }
      >
    | undefined;

    /**
     * @private
     * @property
     */
    private placeholderSprite: AvatarSprite | undefined;

    /**
     * @private
     * @property
     */
    private loadingSprite: AvatarSprite;

    /**
     * Avatar look
     * TODO should we add a add base look?
     *
     * @private
     * @property
     */
    private look: string;

    /**
     * Avatar room position
     *
     * @private
     * @property
     */
    private position$: RoomPosition;

    /**
     * Avatar body direction
     *
     * @private
     * @property
     */
    private direction: number;

    /**
     * Avatar head direction
     *
     * @private
     * @property
     */
    private headDirection?: number;

    /**
     * @private
     * @property
     */
    private moving = false;

    /**
     * @private
     * @property
     */
    private walking = false;

    /**
     * @private
     * @property
     */
    private waving = false;

    /**
     * @private
     * @property
     */
    private item$: string | number | undefined;

    /**
     * @private
     * @property
     */
    private fx: {type: 'dance'; id: string} | undefined;

    /**
     * @private
     * @property
     */
    private animatedPosition: RoomPosition = {roomX: 0, roomY: 0, roomZ: 0};

    /**
     * @private
     * @property
     */
    private frame = 0;

    /**
     * @private
     * @property
     */
    private cancelAnimation: Subscription;

    /**
     * @private
     * @property
     */
    private loadedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
        false
    );

    constructor(private avatarAction: Action, private game: Game) {
        super();
    }

    /**
     * Initialize room avatar
     *
     * @public
     */
    public init({
        look,
        roomX,
        roomY,
        roomZ,
        headDirection,
        direction,
    }: AvatarOptions): void {
        this.look = look;
        this.position$ = {
            roomX,
            roomY,
            roomZ,
        };

        this.headDirection = headDirection;
        this.direction = direction;

        /* this.placeholderSprite = new AvatarSprite({
            look: this._getPlaceholderLookOptions(),
            zIndex: this.calculateZIndex(),
            position: new Point(0, 0),
            onLoad: () => {
                this.updateAvatarSprites();
            },
        });*/

        // this.placeholderSprite.alpha = 0.5;

        // this.avatarSprite = this.placeholderSprite;

        /* this.loadingSprite = new BaseAvatar({
            look: this._getLookOptions(),
            position: { x: 0, y: 0 },
            zIndex: this._calculateZIndex(),
            onLoad: () => {
              this._loaded = true;
              this._updateAvatarSprites();
            },
          });*/

        // this.avatarAction;
    }

    public move(): void {
        throw new Error('Method not implemented.');
    }

    public clearMovement(): void {
        throw new Error('Method not implemented.');
    }

    public destroyed(): void {
        throw new Error('Method not implemented.');
    }

    public registered(): void {
        throw new Error('Method not implemented.');
    }

    /**
     * @private
     */
    private updatePosition(): void {
        if (!this.room) {
            return;
        }

        const {roomX, roomY, roomZ} = this.displayRoomPosition;

        const {x, y} = this.room.getPosition(roomX, roomY, roomZ);

        const roomXrounded = Math.round(roomX);
        const roomYrounded = Math.round(roomY);

        if (this.avatarSprite != null) {
            this.avatarSprite.x = Math.round(x);
            this.avatarSprite.y = Math.round(y);

            const zIndex = this.getZIndexAtPosition(
                roomXrounded,
                roomYrounded,
                this.position.roomZ
            );

            this.avatarSprite.zIndex = zIndex;
            this.avatarSprite.spritesZIndex = zIndex;
        }

        const item = this.room.getTileAt(new Point(roomXrounded, roomYrounded));

        if (item?.type === 'door') {
            this.room.visualization.container.removeChild(this.avatarSprite);
            this.room.visualization.behindWallContainer.addChild(
                this.avatarSprite
            );
        }

        if (item === undefined || item.type !== 'door') {
            this.room.visualization.behindWallContainer.removeChild(
                this.avatarSprite
            );
            this.room.visualization.container.addChild(this.avatarSprite);
        }
    }

    /**
     * @private
     * @param roomX
     * @param roomY
     * @param roomZ
     * @returns {number}
     */
    private getZIndexAtPosition(
        roomX: number,
        roomY: number,
        roomZ: number
    ): number {
        let zOffset = 1;

        if (this.currentLookOptions.actions.has(AvatarAction.Lay)) {
            zOffset += 2000;
        }

        return roomX * 1000 + roomY * 1000 + roomZ + zOffset;
    }

    /**
     * @private
     */
    private updateAvatarSprites(): void {
        if (!this.room) {
            return;
        }

        if (this.loaded) {
            if (this.placeholderSprite !== undefined) {
                this.placeholderSprite.destroy();
            }

            this.placeholderSprite = undefined;

            this.avatarSprite = this.loadingSprite;
        } else if (this.placeholderSprite !== undefined) {
            this.avatarSprite = this.placeholderSprite;
        }

        const look = this.currentLookOptions;
        const animating = true;

        if (animating) {
            this.startAnimation();
        } else {
            this.stopAnimation();
        }

        const avatarSprite = this.avatarSprite;

        if (avatarSprite != null) {
            avatarSprite.lookOptions = look;
        }

        this.updatePosition();
        this.updateEventHandlers();
    }

    /**
     * @private
     */
    private updateFrame(): void {
        this.avatarSprite.currentFrame = this.frame;
    }

    private startAnimation(): void {
        if (this.cancelAnimation !== undefined) {
            return;
        }

        this.frame = 0;
        const start = this.room..current();

        this.cancelAnimation = this.animationTicker.subscribe((value) => {
            this.frame = value - start;
            this.updateFrame();
        });
    }

    /**
     * @private
     */
    private stopAnimation(): void {
        this.frame = 0;
        if (this.cancelAnimation !== undefined) {
            this.cancelAnimation();
            this.cancelAnimation = undefined;
        }
    }

    /**
     * @private
     * @param direction
     * @param headDirection
     */
    private startWalking(direction?: number, headDirection?: number): void {
        this.walking = true;

        if (direction != null) {
            this.direction = direction;
        }

        if (headDirection != null) {
            this.headDirection = headDirection;
        }

        this.updateAvatarSprites();
    }

    /**
     * @private
     */
    private stopWalking(): void {
        this.walking = false;
        this.updateAvatarSprites();
    }

    /**
     * @private
     * @returns {RoomPosition}
     */
    private get displayRoomPosition(): RoomPosition {
        if (this.walking || this.moving) {
            return this.animatedPosition;
        }

        return this.position;
    }

    /**
     * The apparent position of the avatar on the screen. This is useful
     * for placing UI relative to the user.
     *
     * @public
     * @returns {Point}
     */
    public get screenPosition(): Point {
        const worldTransform = this.avatarSprite.worldTransform;
        const point = new Point(0, 0);

        if (worldTransform == null) {
            return point;
        }

        return point.set(worldTransform.tx, worldTransform.ty);
    }

    /**
     * @public
     * @returns {boolean}
     */
    public get loaded(): boolean {
        return this.loadedSubject.value;
    }

    /**
     * @public
     * @returns {Observable<boolean>}
     */
    public get loadedAsObservable(): Observable<boolean> {
        return this.loadedSubject.asObservable();
    }

    /**
     * @public
     * @returns {RoomPosition}
     */
    public get position(): RoomPosition {
        return this.position$;
    }

    /**
     * @public
     */
    public set position(position: RoomPosition) {
        this.position$ = position;
        this.updatePosition();
    }

    /**
     * @public
     * @returns {string | number | undefined}
     */
    public get item(): string | number | undefined {
        return this.item$;
    }

    /**
     * ets the item of the user. Note that this won't have an effect if you don't supply
     * an action which supports items. These are usually `CarryItem` and `UseItem`.
     *
     * @public
     */
    public set item(value: string | number | undefined) {
        this.item$ = value;
        this.updateAvatarSprites();
    }

    /**
     * @public
     * @returns {string | undefined}
     */
    public get dance(): string | undefined {
        if (this.fx?.type === 'dance') {
            return this.fx.id;
        }
    }

    /**
     * @public
     */
    public set dance(value: string | undefined) {
        if (this.fx === undefined || this.fx.type === 'dance') {
            if (value == null) {
                this.fx = undefined;
            } else {
                this.fx = {type: 'dance', id: value};
            }

            this.updateAvatarSprites();
        }
    }

    /**
     * @private
     * @returns {LookOptions}
     */
    private get lookOptions(): LookOptions {
        const combinedActions = new Set<AvatarAction>(
            this.avatarAction.actions
        );

        if (this.walking) {
            combinedActions.add(AvatarAction.Move);
        }

        if (this.waving) {
            combinedActions.add(AvatarAction.Wave);
        }

        if (combinedActions.has(AvatarAction.Lay) && this.walking) {
            combinedActions.delete(AvatarAction.Lay);
        }

        return {
            actions: combinedActions,
            direction: this.direction,
            headDirection: this.headDirection,
            look: this.look,
            item: this.item,
            effect: this.fx,
        };
    }

    /**
     * @private
     * @returns {LookOptions}
     */
    private get placeholderLookOptions(): LookOptions {
        return {
            actions: new Set(),
            direction: this.direction,
            headDirection: this.direction,
            look: 'hd-99999-99999',
            effect: undefined,
            initial: false,
            item: undefined,
        };
    }

    /**
     * @private
     * @returns {LookOptions}
     */
    private get currentLookOptions(): LookOptions {
        if (!this.loaded) {
            return this.placeholderLookOptions;
        }

        return this.lookOptions;
    }
}
