export interface Options {
    resolveImage: (
        id: string,
        library: string
    ) => Promise<HitTexture>;

    createLookServer: () => Promise
}