import {RoomPosition} from '../../Room/Interface/RoomPosition';

/**
 * @interface AvatarOptions
 */
export interface AvatarOptions extends RoomPosition {
    /** Look of the avatar */
    look: string;

    /**
     * Direction of the avatar. Following numbers map to the
     * following directions of the avatar. The `x` would be the location of the
     * avatar and the numbers represent for which number the avatar faces in which direction.
     *
     * ```
     *              x-Axis
     *          x--------------
     *          |
     *          |   7  0  1
     *          |
     *  y-Axis  |   6  x  2
     *          |
     *          |   5  4  3
     *
     * ```
     */
    direction: number;
    headDirection?: number;
}
