import {AvatarAction} from '../Enum/AvatarAction';

/**
 * @interface LookOptions
 */
export interface LookOptions {
    look: string;
    actions: Set<AvatarAction>;
    direction: number;
    headDirection?: number;
    item?: string | number;
    effect?: {type: 'dance' | 'fx'; id: string};
    initial?: boolean;
    skipCaching?: boolean;
}
