/**
 * @type OffsetType
 */
export type OffsetType = (
    fileName: string
  ) => { offsetX: number; offsetY: number } | undefined;