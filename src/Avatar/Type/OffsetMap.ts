/**
 * @type OffsetMap
 */
export type OffsetMap = Map<string, { offsetX: number; offsetY: number }>;