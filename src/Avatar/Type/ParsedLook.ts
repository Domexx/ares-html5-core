/**
 * @type ParsedLook
 */
export type ParsedLook = Map<string, { setId: number; colorId: number }>;