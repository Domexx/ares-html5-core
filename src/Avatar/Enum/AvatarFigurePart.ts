/**
 * @enum AvatarFigurePart
 */
export enum AvatarFigurePart {
    Body = 'bd',
    Shoes = 'sh',
    Legs = 'lg',
    Chest = 'ch',
    WaistAccessory = 'wa',
    ChestAccessory = 'ca',
    Head = 'hd',
    Hair = 'hr',
    FaceAccessory = 'fa',
    EyeAccessory = 'ea',
    HeadAccessory = 'ha',
    HeadAccessoryExtra = 'he',
    CoatCheat = 'cc',
    ChestPrint = 'cp',
    LeftHandItem = 'li',
    LeftHand = 'lh',
    LeftSleeve = 'ls',
    RightHand = 'rh',
    RightSleeve = 'rs',
    Face = 'fc',
    Eyes = 'ey',
    HairBig = 'hrb',
    RightHandItem = 'ri',
    LeftCoatSleeve = 'lc',
    RightCoatSleeve = 'rc'
}