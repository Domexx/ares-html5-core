import {singleton} from 'tsyringe';
import {Log} from '../../Decorator/Log';
import {Logger} from '../../Util/Logger';
import {AvatarAction} from '../Enum/AvatarAction';

@singleton()
/**
 * @class Action
 */
export class Action {
    @Log()
    private logger: Logger;

    /**
     * Avatar holding Actions
     *
     * @private
     * @property
     */
    private actions$: Set<AvatarAction> = new Set<AvatarAction>();

    /**
     * Checks if Avatar has certain Action
     *
     * @param action Avatar Action
     * @returns {boolean}
     */
    public has(action: AvatarAction): boolean {
        return this.actions.has(action);
    }

    /**
     * Remove an action from the active actions.
     *
     * @param action The action to remove
     * @returns {void}
     */
    public remove(action: AvatarAction): void {
        if (!this.actions.delete(action)) {
            this.logger.error(`Could not delete avatar action "${action}"`);
        }

        this.logger.debug(`Deleted avatar action "${action}"`);
    }

    /**
     * Add avatar action
     *
     * @public
     */
    public add(value: AvatarAction): void {
        this.actions$.add(value);
    }

    /**
     * Gets the Avatar Actions
     *
     * @public
     */
    public get actions(): Set<AvatarAction> {
        return this.actions$;
    }
}
