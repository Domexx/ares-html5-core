/**
 * @type Hitmap
 */
export type Hitmap = (
    x: number,
    y: number,
    transform: { x: number; y: number }
) => boolean;