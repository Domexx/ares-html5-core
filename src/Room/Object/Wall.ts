import {
    Container,
    DisplayObject,
    Texture,
    Matrix as PixiMatrix,
    TilingSprite,
    Transform,
    Point,
    Graphics,
} from 'pixi.js';
import {Inject} from '../../Decorator/Inject';
import {WallProps} from '../Interface/WallProps';
import {RoomMatrix} from '../RoomMatrix';
import {RoomObject} from '../RoomObject';
import {RoomPainting} from '../RoomPainting';

/**
 * @class Wall
 * @extends RoomObject
 */
export class Wall extends RoomObject {
    @Inject(RoomMatrix)
    private matrix: RoomMatrix;

    @Inject(RoomPainting)
    private painting: RoomPainting;

    private container: Container | undefined;
    private sprites: DisplayObject[] = [];

    private texture$: Texture | undefined;
    private color$: string | undefined;

    private tileHeight$: number;

    private border$: DisplayObject | undefined;
    private top$: DisplayObject | undefined;
    private primary$: DisplayObject | undefined;

    private wallHeight$: number;
    private wallDepth$: number;
    private hideBorder$: boolean;
    private doorHeight$: number | undefined;

    private wallWidth = 32;

    /**
     * Wall constructor
     *
     * @param props
     */
    constructor(private props: WallProps) {
        super();

        this.texture$ = props.texture;
        this.wallHeight$ = props.wallHeight;
        this.tileHeight$ = props.tileHeight;
        this.wallDepth$ = props.wallDepth;
        this.hideBorder$ = props.hideBorder ?? false;
        this.doorHeight$ = props.doorHeight;
    }

    public destroyed(): void {
        this.border$?.destroy();
        this.top$?.destroy();
        this.primary$?.destroy();
    }
    public registered(): void {
        this.updateSprites();
    }

    private updateSprites() {
        if (!this.room) {
            return;
        }

        this.container?.destroy();
        this.container = new Container();

        const {z, direction, side = true, color} = this.props;

        const {x, y} = this.room.getPosition(this.props.x, this.props.y, z);
        const wallColor = this.color$ ?? color;

        const {leftTint, rightTint, topTint} = this.painting.getWallColors(
            wallColor
        );
        this.container.zIndex = x * 1000 + y * 1000 + z - 1;

        const baseX = x;
        const baseY = y + 16;

        const offset = z * 32;

        switch (direction) {
            case 'left':
                const left = this.createWallLeft({
                    baseX,
                    baseY,
                    wallHeight: this.wallHeight - offset,
                    tileHeight: this.tileHeight,
                    side,
                    offset,
                    borderTint: leftTint,
                    wallTint: rightTint,
                    topTint,
                    borderWidth: this.wallDepth,
                    doorHeight: this.doorHeight$,
                });

                if (!this.hideBorder$) {
                    this.border$ = left.border;
                }
                this.primary$ = left.primary;
                this.top$ = left.top;
                break;

            case 'right':
                const right = this.createWallRight({
                    baseX,
                    baseY,
                    wallHeight: this.wallHeight - z * 32,
                    tileHeight: this.tileHeight,
                    side,
                    offset,
                    borderTint: rightTint,
                    wallTint: leftTint,
                    topTint,
                    borderWidth: this.wallDepth,
                });

                if (!this.hideBorder$) {
                    this.border$ = right.border;
                }

                this.primary$ = right.primary;
                this.top$ = right.top;
                break;

            case 'corner':
                const corner = this.createTopCorner(
                    new PixiMatrix(
                        1,
                        0.5,
                        1,
                        -0.5,
                        baseX + this.wallWidth - this.wallDepth,
                        baseY -
                            this.wallHeight +
                            this.wallWidth / 2 -
                            this.wallDepth / 2 +
                            z * 32
                    ),
                    this.wallDepth
                );

                corner.tint = topTint;

                this.top$ = corner;
                break;
        }

        if (this.container != null) {
            if (this.top$) {
                this.container.addChild(this.top$);
            }

            if (this.border$) {
                this.container.addChild(this.border$);
            }

            if (this.primary$) {
                this.container.addChild(this.primary$);
            }
        }

        this.room.visualization.wallContainer.addChild(this.container);
    }

    private createWall({
        height,
        matrix,
        width = 32,
        offset = 0,
    }: {
        height: number;
        matrix: PixiMatrix;
        width?: number;
        offset?: number;
    }) {
        const sprite = new TilingSprite(this.texture ?? Texture.WHITE);

        const transform = new Transform();
        transform.setFromMatrix(matrix);

        sprite.height = height;
        sprite.width = width;
        sprite.transform = transform;

        sprite.tilePosition = new Point(0, height + offset);

        return sprite;
    }

    private createBorder({
        height,
        matrix,
        borderWidth,
    }: {
        height: number;
        matrix: PixiMatrix;
        borderWidth: number;
    }) {
        const graphics = new Graphics();
        graphics.beginFill(0xffffff);

        // draw a rectangle
        graphics.drawRect(0, 0, borderWidth, height);

        const transform = new Transform();
        transform.setFromMatrix(matrix);

        graphics.transform = transform;

        return graphics;
    }

    private createTopCorner(matrix: PixiMatrix, borderWidth: number) {
        const graphics = new Graphics();
        graphics.beginFill(0xffffff);

        // draw a rectangle
        graphics.drawRect(0, 0, borderWidth, borderWidth);

        const transform = new Transform();
        transform.setFromMatrix(matrix);

        graphics.transform = transform;

        return graphics;
    }

    private createTopBorder(matrix: PixiMatrix, borderWidth: number) {
        const graphics = new Graphics();
        graphics.beginFill(0xffffff);

        // draw a rectangle
        graphics.drawRect(0, 0, borderWidth, this.wallWidth);

        const transform = new Transform();
        transform.setFromMatrix(matrix);

        graphics.transform = transform;

        return graphics;
    }

    private createWallLeft({
        baseX,
        baseY,
        wallHeight,
        tileHeight,
        side,
        offset,
        borderTint,
        topTint,
        wallTint,
        borderWidth,
        doorHeight,
    }: {
        baseX: number;
        baseY: number;
        wallHeight: number;
        tileHeight: number;
        side: boolean;
        offset: number;
        borderTint: number;
        wallTint: number;
        topTint: number;
        borderWidth: number;
        doorHeight?: number;
    }) {
        const top = this.createTopBorder(
            new PixiMatrix(
                1,
                0.5,
                1,
                -0.5,
                baseX + this.wallWidth - borderWidth,
                baseY - wallHeight + this.wallWidth / 2 - borderWidth / 2
            ),
            borderWidth
        );

        const border = this.createBorder({
            height: wallHeight + tileHeight,
            matrix: new PixiMatrix(
                1,
                0.5,
                0,
                1,
                baseX + this.wallWidth - borderWidth,
                baseY - wallHeight + this.wallWidth / 2 - borderWidth / 2
            ),
            borderWidth,
        });

        const actualWallHeight = doorHeight != null ? doorHeight : wallHeight;
        const actualOffset =
            doorHeight != null
                ? (offset ?? 0) + (wallHeight - doorHeight)
                : offset ?? 0;

        const primary = this.createWall({
            height: actualWallHeight,
            matrix: new PixiMatrix(
                -1,
                0.5,
                0,
                1,
                baseX + 2 * this.wallWidth,
                baseY - wallHeight
            ),
            offset: actualOffset,
        });

        border.tint = borderTint;
        primary.tint = wallTint;
        top.tint = topTint;

        return {
            border: side ? border : undefined,
            primary,
            top,
        };
    }

    private createWallRight({
        baseX,
        baseY,
        wallHeight,
        tileHeight,
        side,
        offset,
        borderTint,
        topTint,
        wallTint,
        borderWidth,
    }: {
        baseX: number;
        baseY: number;
        wallHeight: number;
        tileHeight: number;
        side: boolean;
        offset: number;
        borderTint: number;
        wallTint: number;
        topTint: number;
        borderWidth: number;
    }) {
        const top = this.createTopBorder(
            new PixiMatrix(1, -0.5, 1, 0.5, baseX, baseY - wallHeight),
            borderWidth
        );

        const border = this.createBorder({
            height: wallHeight + tileHeight,
            matrix: new PixiMatrix(
                -1,
                0.5,
                0,
                1,
                baseX + this.wallWidth + borderWidth,
                baseY - wallHeight + this.wallWidth / 2 - borderWidth / 2
            ),
            borderWidth,
        });

        const primary = this.createWall({
            height: wallHeight,
            matrix: new PixiMatrix(1, 0.5, 0, 1, baseX, baseY - wallHeight),
            offset,
        });

        border.tint = borderTint;
        primary.tint = wallTint;
        top.tint = topTint;

        return {
            border: side ? border : undefined,
            primary,
            top,
        };
    }

    public get wallDepth(): number {
        return this.wallDepth$;
    }

    public set wallDepth(value: number) {
        this.wallDepth$ = value;
        this.updateSprites();
    }

    public get texture(): Texture | undefined {
        return this.texture$;
    }

    public set texture(value: Texture | undefined) {
        this.texture$ = value;
        this.updateSprites();
    }

    public get color(): string | undefined {
        return this.color$;
    }

    public set color(value: string | undefined) {
        this.color$ = value;
        this.updateSprites();
    }

    public get wallHeight(): number {
        return this.wallHeight$;
    }

    public set wallHeight(value: number) {
        this.wallHeight$ = value;
        this.updateSprites();
    }

    public get tileHeight(): number {
        return this.tileHeight$;
    }

    public set tileHeight(value: number) {
        this.tileHeight$ = value;
        this.updateSprites();
    }
}
