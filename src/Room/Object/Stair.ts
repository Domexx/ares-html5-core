import {
    Container,
    DisplayObject,
    Texture,
    Matrix as PixiMatrix,
    Point,
    TilingSprite,
} from 'pixi.js';
import {Inject} from '../../Decorator/Inject';
import {StairProps, StairBoxProps} from '../Interface/StairProps';
import {RoomMatrix} from '../RoomMatrix';
import {RoomObject} from '../RoomObject';
import {RoomPainting} from '../RoomPainting';

/**
 * @class Stair
 */
export class Stair extends RoomObject {
    @Inject(RoomMatrix)
    private matrix: RoomMatrix;

    @Inject(RoomPainting)
    private painting: RoomPainting;

    private container: Container | undefined;
    private sprites: DisplayObject[] = [];

    private texture$: Texture | undefined;
    private color$: string | undefined;

    private tileHeight$: number;

    private stairBase = 8;

    constructor(private props: StairProps) {
        super();

        this.texture$ = props.texture;
        this.tileHeight = props.tileHeight;
    }

    destroyed(): void {
        this.destroySprites();
    }

    registered(): void {
        this.updateSprites();
    }

    private destroySprites(): void {
        this.container?.destroy();
    }

    private updateSprites(): void {
        if (!this.room) {
            return;
        }

        this.container?.destroy();
        this.container = new Container();

        const {z, color, direction} = this.props;
        this.container.zIndex = this.props.x * 1000 + this.props.y * 1000 + z;

        const {x, y} = this.room.getPosition(this.props.x, this.props.y, z);

        for (let i = 0; i < 4; i++) {
            const props = {
                x,
                y,
                tileHeight: this.tileHeight,
                index: 3 - i,
                color: this.color ?? color,
                tilePositions: this.room.roomMap.getTilePositionForTile(
                    new Point(x, y)
                ),
            };

            if (direction === 0) {
                this.container.addChild(
                    ...this.createStairBoxDirection0(props)
                );
            } else if (direction === 2) {
                this.container.addChild(
                    ...this.createStairBoxDirection2(props)
                );
            }
        }

        this.room.visualization.floorContainer.addChild(this.container);
    }

    private createSprite(
        matrix: PixiMatrix,
        tint: number,
        tilePosition?: Point,
        texture?: Texture
    ) {
        const tile = new TilingSprite(texture ?? this.texture ?? Texture.WHITE);
        tile.tilePosition = tilePosition ?? new Point(0, 0);
        tile.transform.setFromMatrix(matrix);

        tile.tint = tint;

        return tile;
    }

    private createStairBoxDirection0({
        x,
        y,
        tileHeight,
        index,
        color,
        tilePositions,
    }: StairBoxProps): DisplayObject[] {
        const baseX = x + this.stairBase * index;
        const baseY = y - this.stairBase * index * 1.5;

        const basePoint: Point = new Point(baseX, baseY);

        const {
            tileTint,
            borderRightTint,
            borderLeftTint,
        } = this.painting.getTileColors(color);

        const tile = this.createSprite(
            this.matrix.getFloorMatrix(basePoint),
            tileTint,
            tilePositions.top
        );
        tile.width = 32;
        tile.height = 8;

        const borderLeft = this.createSprite(
            this.matrix.getLeftMatrix(basePoint, {
                width: 32,
                height: tileHeight,
            }),
            borderLeftTint,
            tilePositions.left
        );
        borderLeft.width = 32;
        borderLeft.height = tileHeight;

        const borderRight = this.createSprite(
            this.matrix.getRightMatrix(basePoint, {
                width: 8,
                height: tileHeight,
            }),
            borderRightTint,
            tilePositions.right
        );

        borderRight.width = 8;
        borderRight.height = tileHeight;

        return [borderLeft, borderRight, tile];
    }

    private createStairBoxDirection2({
        x,
        y,
        tileHeight,
        index,
        color,
    }: StairBoxProps) {
        const baseX = x - this.stairBase * index;
        const baseY = y - this.stairBase * index * 1.5;

        const {
            tileTint,
            borderRightTint,
            borderLeftTint,
        } = this.painting.getTileColors(color);

        const tile = this.createSprite(
            this.matrix.getFloorMatrix(
                new Point(
                    baseX + 32 - this.stairBase,
                    baseY + this.stairBase * 1.5
                )
            ),
            tileTint
        );
        tile.width = this.stairBase;
        tile.height = 32;

        const borderLeft = this.createSprite(
            this.matrix.getLeftMatrix(
                new Point(
                    baseX + 32 - this.stairBase,
                    baseY + this.stairBase * 1.5
                ),
                {
                    width: this.stairBase,
                    height: tileHeight,
                }
            ),
            borderLeftTint
        );
        borderLeft.width = this.stairBase;
        borderLeft.height = tileHeight;

        const borderRight = this.createSprite(
            this.matrix.getRightMatrix(new Point(baseX, baseY), {
                width: 32,
                height: tileHeight,
            }),
            borderRightTint
        );

        borderRight.width = 32;
        borderRight.height = tileHeight;

        return [borderLeft, borderRight, tile];
    }

    public get texture(): Texture | undefined {
        return this.texture$;
    }

    public set texture(value: Texture | undefined) {
        this.texture$ = value;
        this.updateSprites();
    }

    public get tileHeight(): number {
        return this.tileHeight$;
    }

    public set tileHeight(value: number) {
        this.tileHeight$ = value;
        this.updateSprites();
    }

    public get color(): string | undefined {
        return this.color$;
    }

    public set color(value: string | undefined) {
        this.color$ = value;
        this.updateSprites();
    }
}
