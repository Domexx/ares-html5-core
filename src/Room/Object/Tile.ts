import {Container, DisplayObject, Point, Texture, TilingSprite} from 'pixi.js';
import {Inject} from '../../Decorator/Inject';
import {TileProps} from '../Interface/TileProps';
import {RoomMatrix} from '../RoomMatrix';
import {RoomObject} from '../RoomObject';
import {RoomPainting} from '../RoomPainting';

/**
 * @class Tile
 */
export class Tile extends RoomObject {
    @Inject(RoomPainting)
    /**
     * @private
     * @property
     */
    private painting: RoomPainting;

    @Inject(RoomMatrix)
    /**
     * @private
     * @property
     */
    private matrix: RoomMatrix;

    /**
     * @private
     * @property
     */
    private container: Container | undefined;

    /**
     * @private
     * @property
     */
    private sprites: DisplayObject[] = [];

    /**
     * @private
     * @property
     */
    private texture$: Texture | undefined;

    /**
     * @private
     * @property
     */
    private color$: string | undefined;

    /**
     * @private
     * @property
     */
    private tileHeight$: number;

    /**
     * @private
     * @property
     */
    private door: boolean;

    /**
     * Tile constructor
     *
     * @param props
     */
    constructor(private props: TileProps) {
        super();

        this.texture$ = props.texture;
        this.color$ = props.color;
        this.tileHeight$ = props.tileHeight;
        this.door = props.door ?? false;
    }

    /**
     * Destroys created Sprites
     *
     * @returns {void}
     */
    public destroyed(): void {
        this.destroySprites();
    }

    /**
     * Updates registered Sprites
     *
     * @returns {void}
     */
    public registered(): void {
        this.updateSprites();
    }

    /**
     * Destroys created Sprites
     *
     * @returns {void}
     */
    private destroySprites(): void {
        this.sprites.forEach((sprite) => sprite.destroy());
        this.sprites = [];
    }

    /**
     * Updates the Sprites
     *
     * @returns {void}
     */
    private updateSprites(): void {
        this.container?.destroy();
        this.container = new Container();

        this.destroySprites();

        const point = this.room.getPosition(
            this.props.x,
            this.props.y,
            this.props.z
        );

        const {x, y} = {
            x: point.x,
            y: point.y,
        };

        this.container.zIndex = x * 1000 + y * 1000 + this.props.z;

        const {
            borderLeftTint,
            borderRightTint,
            tileTint,
        } = this.painting.getTileColors(this.color$ ?? this.props.color);

        const tileMatrix = this.matrix.getFloorMatrix(point);

        const tilePositions = this.room.roomMap.getTilePositionForTile(
            new Point(this.room.x, this.room.y)
        );

        const tile = new TilingSprite(this.texture ?? Texture.WHITE);
        tile.tilePosition = tilePositions.top;

        tile.transform.setFromMatrix(tileMatrix);
        tile.width = 32;
        tile.height = 32;
        tile.tint = tileTint;

        const borderLeftMatrix = this.matrix.getLeftMatrix(point, {
            width: 32,
            height: this.tileHeight,
        });

        const borderRightMatrix = this.matrix.getRightMatrix(point, {
            width: 32,
            height: this.tileHeight,
        });

        const borderLeft = new TilingSprite(this.texture ?? Texture.WHITE);
        borderLeft.transform.setFromMatrix(borderLeftMatrix);
        borderLeft.width = 32;
        borderLeft.height = this.tileHeight;
        borderLeft.tilePosition = tilePositions.left;
        borderLeft.tint = borderLeftTint;

        const borderRight = new TilingSprite(this.texture ?? Texture.WHITE);
        borderRight.transform.setFromMatrix(borderRightMatrix);
        borderRight.width = 32;
        borderRight.height = this.tileHeight;
        borderRight.tilePosition = tilePositions.right;
        borderRight.tint = borderRightTint;

        this.sprites.push(this.container);

        this.container.addChild(borderLeft);
        this.container.addChild(borderRight);
        this.container.addChild(tile);

        if (!this.door) {
            this.room.visualization.floorContainer.addChild(this.container);
        } else {
            this.room.visualization.behindWallContainer.addChild(
                this.container
            );
            this.container.zIndex = -1;
        }
    }

    /**
     * Gets the Tile Height
     *
     * @returns {number}
     */
    public get tileHeight(): number {
        return this.tileHeight$;
    }

    /**
     * Sets the Tile Height
     *
     * @param value
     */
    public set tileHeight(value: number) {
        this.tileHeight$ = value;
        this.updateSprites();
    }

    /**
     * Gets the Tile Texture
     */
    public get texture(): Texture | undefined {
        return this.texture$;
    }

    /**
     * Sets the Tile Texture
     */
    public set texture(value: Texture | undefined) {
        this.texture$ = value;
        this.updateSprites();
    }

    /**
     * Gets the Tile Color
     */
    public get color(): string | undefined {
        return this.color$;
    }

    /**
     * Sets the Tile Color
     */
    public set color(value: string | undefined) {
        this.color$ = value;
        this.updateSprites();
    }
}
