import {Graphics, Point, Polygon} from 'pixi.js';
import {RoomPosition} from '../Interface/RoomPosition';
import {RoomObject} from '../RoomObject';

/**
 * @class TileCursor
 * @extends RoomObject
 */
export class TileCursor extends RoomObject {
    /**
     * @private
     * @property
     */
    private roomX: number;

    /**
     * @private
     * @property
     */
    private roomY: number;

    /**
     * @private
     * @property
     */
    private roomZ: number;

    /**
     * @private
     * @property
     */
    private graphics: Graphics | undefined;

    /**
     * @private
     * @property
     */
    private hover$ = false;

    /**
     * @private
     * @property
     */
    private position: RoomPosition;

    /**
     * @private
     * @property
     */
    private door: boolean;

    /**
     * @private
     * @property
     */
    private points: {[key: string]: Point} = {
        p1: new Point(0, 16),
        p2: new Point(32, 0),
        p3: new Point(64, 16),
        p4: new Point(32, 32),
    };

    /**
     * TileCursor constructir
     *
     * @param position
     * @param door
     */
    constructor(position: RoomPosition, door = true) {
        super();

        this.roomX = position.roomX;
        this.roomY = position.roomY;
        this.roomZ = position.roomZ;
        this.position = position;
        this.door = door;
    }

    /**
     * @public
     */
    public destroyed(): void {
        this.graphics?.destroy();
    }

    /**
     * @public
     */
    public registered(): void {
        this.graphics = this.createGraphics();

        if (!this.door) {
            this.room.visualization.behindWallContainer.addChild(this.graphics);
        } else {
            this.room.visualization.container.addChild(this.graphics);
        }

        this.updateGraphics();
    }

    /**
     * @private
     * @param position
     */
    public onOver(
        position: RoomPosition,
        callback?: (pos: RoomPosition) => void
    ): void {
        if (callback) {
            return callback(position);
        }
    }

    /**
     * @private
     * @param position
     */
    public onOut(
        position: RoomPosition,
        callback?: (pos: RoomPosition) => void
    ): void {
        if (callback) {
            return callback(position);
        }
    }

    /**
     * @private
     * @param position
     */
    public onClick(
        position: RoomPosition,
        callback?: (pos: RoomPosition) => void
    ): void {
        if (callback) {
            return callback(position);
        }
    }

    /**
     * @private
     */
    private updateGraphics(): void {
        if (this.graphics === undefined) {
            return;
        }

        this.graphics.clear();

        const {x, y} = this.room.getPosition(
            this.roomX,
            this.roomY,
            this.roomZ
        );

        this.graphics.zIndex = this.door
            ? 0
            : this.roomX * 1000 + this.roomY * 1000 + this.roomZ - 1000;
        this.graphics.x = x;
        this.graphics.y = y;

        if (this.hover) {
            this.drawBorder(0x000000, 0.33, 0);
            this.drawBorder(0xa7d1e0, 1, -2);
            this.drawBorder(0xffffff, 1, -3);
        }
    }

    /**
     * @private
     * @returns {Graphics}
     */
    private createGraphics(): Graphics {
        const graphics = new Graphics();

        graphics.hitArea = new Polygon([
            new Point(this.points.p1.x, this.points.p1.y),
            new Point(this.points.p2.x, this.points.p2.y),
            new Point(this.points.p3.x, this.points.p3.y),
            new Point(this.points.p4.x, this.points.p4.y),
        ]);

        graphics.interactive = true;

        graphics.addListener('mouseover', () => (this.hover = true));
        graphics.addListener('mouseout', () => (this.hover = false));
        graphics.addListener('click', () => this.onClick(this.position));

        return graphics;
    }

    /**
     * @private
     * @param color
     * @param alpha
     * @param offsetY
     */
    private drawBorder(color: number, alpha = 1, offsetY: number): void {
        if (this.graphics === undefined) {
            return;
        }

        this.graphics.beginFill(color, alpha);
        this.graphics.moveTo(this.points.p1.x, this.points.p1.y + offsetY);
        this.graphics.lineTo(this.points.p2.x, this.points.p2.y + offsetY);
        this.graphics.lineTo(this.points.p3.x, this.points.p3.y + offsetY);
        this.graphics.lineTo(this.points.p4.x, this.points.p4.y + offsetY);
        this.graphics.endFill();

        this.graphics.beginHole();
        this.graphics.moveTo(this.points.p1.x + 6, this.points.p1.y + offsetY);
        this.graphics.lineTo(this.points.p2.x, this.points.p2.y + 3 + offsetY);
        this.graphics.lineTo(this.points.p3.x - 6, this.points.p3.y + offsetY);
        this.graphics.lineTo(this.points.p4.x, this.points.p4.y - 3 + offsetY);
        this.graphics.endHole();
    }

    /**
     * @public
     */
    public get hover(): boolean {
        return this.hover$;
    }

    /**
     * @public
     */
    public set hover(value: boolean) {
        if (this.hover === value) {
            return;
        }

        this.hover$ = value;
        this.updateGraphics();

        if (value) {
            this.onOver(this.position);
            return;
        }

        this.onOut(this.position);
    }
}
