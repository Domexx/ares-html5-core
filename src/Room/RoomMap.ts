import {Point} from 'pixi.js';
import {injectable} from 'tsyringe';
import {Padding} from './Error/Padding';
import {DoorType} from './Interface/DoorType';
import {HiddenType} from './Interface/HiddenType';
import {StairType} from './Interface/StairType';
import {TileInfo} from './Interface/TileInfo';
import {TileType} from './Interface/TileType';
import {ColumnWall} from './Interface/Wall/ColumnWall';
import {RowWall} from './Interface/Wall/RowWall';
import {WallType} from './Interface/WallType';
import {MapTileType} from './Type/Tile';

@injectable()
/**
 * @class RoomMap
 */
export class RoomMap {
    /**
     * @private
     * @property
     */
    private offsets: {[key: string]: Point} = {
        none: new Point(0, 0),
        top: new Point(0, -1),
        left: new Point(-1, 0),
    };

    /**
     * @private
     * @property
     */
    private rowWalls: Map<string, RowWall> = new Map<string, RowWall>();

    /**
     * @private
     * @property
     */
    private colWalls: Map<string, ColumnWall> = new Map<string, ColumnWall>();

    /**
     * Parse map from string
     *
     * @param str
     * @returns {MapTileType[]}
     */
    public parseMapFromString(map: string): MapTileType[][] {
        map = map.replace(/\r/g, '\n');
        map = map.replace(/ /g, '');

        return map
            .split('\n')
            .map((row) => row.trim())
            .filter((row) => row.length > 0)
            .map((row) =>
                row.split('').map((value: string) => value as MapTileType)
            );
    }

    /**
     * Parse tile map
     *
     * @param map
     */
    public parseMap(
        map: MapTileType[][]
    ): {
        map: (DoorType | HiddenType | StairType | TileType | WallType)[][];
        largestDiff: number;
        wallOffsets: Point;
        positionOffsets: Point;
        maskOffsets: Point;
    } {
        this.getRowWalls(map).forEach((row: RowWall) => {
            for (let y = row.startY; y <= row.endY; y++) {
                this.rowWalls.set(`${row.x}_${y}`, row);
            }
        });

        this.getColumnWalls(map).forEach((column: ColumnWall) => {
            for (let x = column.startX; x <= column.endX; x++) {
                this.colWalls.set(`${x}_${column.y}`, column);
            }
        });

        this.assertMapHasPadding(map);

        const result: (
            | DoorType
            | HiddenType
            | StairType
            | TileType
            | WallType
        )[][] = map.map((row) =>
            row.map(() => ({
                type: 'hidden',
            }))
        );

        let lowestTile: number | undefined;
        let highestTile: number | undefined;
        let hasDoor = false;

        const applyHighLowTile = (current: number) => {
            if (highestTile == null || current > highestTile) {
                highestTile = current;
            }

            if (lowestTile == null || current < lowestTile) {
                lowestTile = current;
            }
        };

        for (let y = 0; y < map.length; y++) {
            for (let x = 0; x < map[y].length; x++) {
                const point = new Point(x, y);

                const resultX = x;
                const resultY = y;

                const tileInfo = this.getTileInfo(map, point);
                const tileInfoBelow = this.getTileInfo(
                    map,
                    point.clone().set(x, y + 1)
                );
                const tileInfoRight = this.getTileInfo(
                    map,
                    point.clone().set(x + 1, y)
                );

                const wall = this.getWall(point);

                if (wall !== undefined) {
                    switch (wall.kind) {
                        case 'column':
                            const colWallHeightDiff = !Number.isNaN(
                                tileInfoBelow.height
                            )
                                ? Math.abs(tileInfoBelow.height - wall.height)
                                : 0;

                            result[resultY][resultX] = {
                                kind: 'colWall',
                                type: 'wall',
                                height: wall.height,
                                hideBorder: colWallHeightDiff > 0,
                            };
                            break;

                        case 'row':
                            const rowWallHeightDiff = !Number.isNaN(
                                tileInfoBelow.height
                            )
                                ? Math.abs(tileInfoRight.height - wall.height)
                                : 0;

                            result[resultY][resultX] = {
                                kind: 'rowWall',
                                type: 'wall',
                                height: wall.height,
                                hideBorder:
                                    tileInfoBelow.rowDoor ||
                                    rowWallHeightDiff > 0,
                            };
                            break;

                        case 'innerCorner':
                            result[resultY][resultX] = {
                                kind: 'innerCorner',
                                type: 'wall',
                                height: wall.height,
                            };
                            break;

                        case 'outerCorner':
                            result[resultY][resultX] = {
                                kind: 'outerCorner',
                                type: 'wall',
                                height: wall.height,
                            };
                            break;
                    }
                }

                if (!tileInfo.rowDoor || hasDoor) {
                    if (
                        tileInfo.stairs !== undefined &&
                        !Number.isNaN(tileInfo.height)
                    ) {
                        result[resultY][resultX] = {
                            type: 'stairs',
                            kind: tileInfo.stairs.direction,
                            z: tileInfo.height,
                        };

                        applyHighLowTile(tileInfo.height);
                    } else if (!Number.isNaN(tileInfo.height)) {
                        result[resultY][resultX] = {
                            type: 'tile',
                            z: tileInfo.height,
                        };
                        applyHighLowTile(tileInfo.height);
                    }
                } else {
                    hasDoor = true;
                    result[resultY][resultX] = {
                        type: 'door',
                        z: tileInfo.height,
                    };
                }
            }
        }

        let largestDiff = 0;

        if (lowestTile != null && highestTile != null) {
            largestDiff = highestTile - lowestTile;
        }

        const wallOffsets: Point = new Point(1, 1);

        return {
            map: result,
            largestDiff,
            wallOffsets,
            positionOffsets: new Point(0, 0),
            maskOffsets: new Point(-wallOffsets.x, -wallOffsets.y),
        };
    }

    /**
     * Get tile
     *
     * @param tiles
     * @param point
     * @param offset
     * @returns {number | 'x'}
     */
    public getTile(
        tiles: MapTileType[][],
        point: Point,
        offset = 'none'
    ): number | 'x' {
        const {x, y} = {
            x: point.x + this.offsets[offset].x,
            y: point.y + this.offsets[offset].y,
        };

        if (tiles[y] === undefined) {
            return 'x';
        }

        if (tiles[y][x] === undefined) {
            return 'x';
        }

        return this.getNumberOfTileType(tiles[y][x]);
    }

    /**
     * Get tile information
     *
     * @param tiles
     * @param point
     * @returns {TileInfo}
     */
    public getTileInfo(tiles: MapTileType[][], point: Point): TileInfo {
        const {x, y} = {x: point.x, y: point.y};
        const type = this.getTile(tiles, point);

        const leftType = this.getTile(tiles, new Point(x - 1, y));
        const topType = this.getTile(tiles, new Point(x, y - 1));

        const topLeftDiagonalType = this.getTile(
            tiles,
            new Point(x - 1, y - 1)
        );
        const bottomLeftDiagonalType = this.getTile(
            tiles,
            new Point(x - 1, y + 1)
        );
        const bottomType = this.getTile(tiles, new Point(x, y + 1));
        const rightType = this.getTile(tiles, new Point(x + 1, y));

        // A row door can be identified if its surrounded by nothing to the left, top and bottom.
        const rowDoor =
            topType === 'x' &&
            leftType === 'x' &&
            topLeftDiagonalType === 'x' &&
            bottomType === 'x' &&
            bottomLeftDiagonalType === 'x' &&
            this.isTile(rightType) &&
            this.isTile(type);

        const stairs = this.getStairs(tiles, new Point(x, y));
        const baseHeight = this.isTile(type) ? type : undefined;

        return {
            rowEdge: leftType === 'x' && this.isTile(type),
            colEdge: topType === 'x' && this.isTile(type),
            innerEdge:
                topLeftDiagonalType === 'x' &&
                this.isTile(type) &&
                this.isTile(topType) &&
                this.isTile(leftType),
            stairs,
            height: Number(baseHeight),
            rowDoor,
        };
    }

    /**
     * Get stairs from map
     *
     * @param tiles
     * @param point
     */
    public getStairs(
        tiles: MapTileType[][],
        point: Point
    ): {direction: number} | undefined {
        const {x, y} = {x: point.x, y: point.y};

        const type = this.getTile(tiles, point);
        const topType = this.getTile(tiles, point, 'top');
        const leftType = this.getTile(tiles, point, 'left');

        if (this.isTile(topType) && this.isTile(type)) {
            const diff = Number(topType) - Number(type);

            if (diff === 1) {
                const destinationStairs = this.getStairs(
                    tiles,
                    new Point(x + this.offsets.top.x, y + this.offsets.top.y)
                );

                if (
                    destinationStairs === undefined ||
                    destinationStairs.direction === 0
                ) {
                    return {direction: 0};
                }
            }
        }

        if (this.isTile(leftType) && this.isTile(type)) {
            const diff = Number(leftType) - Number(type);

            if (diff === 1) {
                const destinationStairs = this.getStairs(
                    tiles,
                    new Point(x + this.offsets.left.x, y + this.offsets.left.y)
                );

                if (
                    destinationStairs == null ||
                    destinationStairs.direction === 2
                ) {
                    return {direction: 2};
                }
            }
        }
    }

    /**
     * Get wall
     *
     * @param point
     * @returns {{kind: string, height: number} | undefined}
     */
    public getWall(point: Point): {kind: string; height: number} | undefined {
        const {x, y} = {x: point.x, y: point.y};

        const rightColWall = this.getColumnWall(new Point(x + 1, y));
        const bottomRowWall = this.getRowWall(new Point(x, y + 1));

        if (rightColWall !== undefined && bottomRowWall !== undefined) {
            // This is a outer corner
            return {
                kind: 'outerCorner',
                height: Math.min(rightColWall.height, bottomRowWall.height),
            };
        }

        const leftColWall = this.getColumnWall(point);
        const topRowWall = this.getRowWall(point);

        if (leftColWall !== undefined && topRowWall !== undefined) {
            return {
                kind: 'innerCorner',
                height: Math.min(leftColWall.height, topRowWall.height),
            };
        }

        const rowWall = this.getRowWall(point);

        if (rowWall !== undefined) {
            return {
                kind: 'row' as const,
                height: rowWall.height,
            };
        }

        const colWall = this.getColumnWall(point);

        if (colWall !== undefined) {
            return {
                kind: 'column',
                height: colWall.height,
            };
        }
    }

    /**
     * @public
     * @param type
     * @returns {boolean}
     */
    public isTile(type: number | 'x'): boolean {
        return !Number.isNaN(Number(type));
    }

    /**
     * @public
     * @returns {Point}
     */
    public getTilePosition(point: Point): Point {
        const xEven = point.x % 2 === 0;
        const yEven = point.y % 2 === 0;

        return new Point(xEven ? 0 : 32, yEven ? 32 : 0);
    }

    /**
     * @public
     */
    public getTilePositionForTile(
        point: Point
    ): {top: Point; left: Point; right: Point} {
        return {
            top: this.getTilePosition(point),
            left: this.getTilePosition(new Point(point.x, point.y + 1)),
            right: this.getTilePosition(new Point(point.x + 1, point.y)),
        };
    }

    /**
     *
     * @public
     * @param map
     * @param wallOffsets
     */
    public getMapBounds(
        map: (DoorType | HiddenType | StairType | TileType | WallType)[][],
        wallOffsets: Point
    ): {
        minX: number;
        minY: number;
        maxX: number;
        maxY: number;
    } {
        let minX: number | undefined;
        let minY: number | undefined;

        let maxX: number | undefined;
        let maxY: number | undefined;

        map.forEach((row, y) => {
            row.forEach((column, x) => {
                if (column.type !== 'tile') {
                    return;
                }

                const position = this.getPosition(x, y, column.z, wallOffsets);
                const localMaxX = position.x + 64;
                const localMaxY = position.y + 32;

                if (minX === undefined || position.x < minX) {
                    minX = position.x;
                }

                if (minY === undefined || position.y < minY) {
                    minY = position.y;
                }

                if (maxX === undefined || localMaxX > maxX) {
                    maxX = localMaxX;
                }

                if (maxY === undefined || localMaxY > maxY) {
                    maxY = localMaxY;
                }
            });
        });

        if (
            minX === undefined ||
            minY === undefined ||
            maxX === undefined ||
            maxY === undefined
        ) {
            throw new Error('Couldnt figure out dimensions');
        }

        return {
            minX,
            minY: minY - 32,
            maxX,
            maxY: maxY - 32,
        };
    }

    /**
     * Get position
     *
     * @param roomX
     * @param roomY
     * @param roomZ
     * @param wallOffsets
     * @returns {Point}
     */
    public getPosition(
        roomX: number,
        roomY: number,
        roomZ: number,
        wallOffsets: Point
    ): Point {
        roomX = roomX + wallOffsets.x;
        roomY = roomY + wallOffsets.y;

        const base = 32;

        const xPos = roomX * base - roomY * base;
        const yPos = roomX * (base / 2) + roomY * (base / 2);

        return new Point(xPos, yPos - roomZ * 32);
    }

    /**
     * @private
     * @param tileType
     * @returns {number | 'x'}
     */
    private getNumberOfTileType(tileType: MapTileType): number | 'x' {
        if (tileType === 'x' || tileType === undefined) {
            return 'x';
        }

        const parsedNumber = Number(tileType);

        if (Number.isNaN(parsedNumber)) {
            const offset = 9;

            return Number(tileType.charCodeAt(0)) - 96 + offset;
        }

        return parsedNumber;
    }

    /**
     * @private
     * @param map
     */
    private assertMapHasPadding(map: MapTileType[][]): void {
        if (map.length < 1) {
            throw new Error('Map has no rows');
        }

        let doorCount = 0;

        for (let y = 0; y < map.length; y++) {
            const row = map[y];

            if (row.length < 1) {
                throw new Error('Tilemap row was empty.');
            }

            if (row[0] !== 'x') {
                if (doorCount > 0) {
                    throw new Padding(`row ${y}`);
                }

                doorCount++;
            }
        }

        for (let x = 0; x < map[0].length; x++) {
            const cell = map[0][x];

            if (cell !== 'x') {
                throw new Padding(`column ${x}`);
            }
        }
    }

    /**
     * Get row walls
     *
     * @param map
     * @returns {RowWall[]}
     */
    private getRowWalls(map: MapTileType[][]): RowWall[] {
        let lastY = map.length - 1;

        let wallEndY: number | undefined;
        let wallStartY: number | undefined;
        let height: number | undefined;

        const walls: RowWall[] = [];

        for (let x = 0; x < map[0].length; x++) {
            for (let y = lastY; y >= 0; y--) {
                const point: Point = new Point(x, y);
                const current = this.getTileInfo(map, point);

                if (current.rowEdge && !current.rowDoor) {
                    if (wallEndY === undefined) {
                        wallEndY = y;
                    }

                    wallStartY = y;
                    lastY = y - 1;

                    if (
                        height === undefined ||
                        (current.height ?? 0) < height
                    ) {
                        height = current.height;
                    }
                } else {
                    if (wallEndY !== undefined && wallStartY !== undefined) {
                        walls.push({
                            startY: wallStartY,
                            endY: wallEndY,
                            x: x - 1,
                            height: height ?? 0,
                        });
                        wallEndY = undefined;
                        wallStartY = undefined;
                        height = undefined;
                    }
                }
            }
        }

        return walls;
    }

    /**
     * Gets the Column Walls
     *
     * @param map
     * @returns {ColumnWall[]}
     */
    private getColumnWalls(map: MapTileType[][]): ColumnWall[] {
        let lastX = map[0].length - 1;

        let wallEndX: number | undefined;
        let wallStartX: number | undefined;
        let height: number | undefined;

        const walls: ColumnWall[] = [];

        for (let y = 0; y < map.length; y++) {
            for (let x = lastX; x >= 0; x--) {
                const point: Point = new Point(x, y);
                const current = this.getTileInfo(map, point);

                if (current.colEdge && !current.rowDoor) {
                    if (wallEndX === undefined) {
                        wallEndX = x;
                    }

                    wallStartX = x;
                    lastX = x - 1;
                    if (
                        height === undefined ||
                        (current.height ?? 0) < height
                    ) {
                        height = current.height;
                    }
                } else {
                    if (wallEndX !== undefined && wallStartX !== undefined) {
                        walls.push({
                            startX: wallStartX,
                            endX: wallEndX,
                            y: y - 1,
                            height: height ?? 0,
                        });
                        wallEndX = undefined;
                        wallStartX = undefined;
                        height = undefined;
                    }
                }
            }
        }

        return walls;
    }

    /**
     * Get row wall
     *
     * @param point
     * @returns {RowWall | undefined}
     */
    private getRowWall(point: Point): RowWall | undefined {
        return this.rowWalls.get(`${point.x}_${point.y}`);
    }

    /**
     * Get column wall
     *
     * @param point
     * @returns {ColumnWall | undefined}
     */
    private getColumnWall(point: Point): ColumnWall | undefined {
        return this.colWalls.get(`${point.x}_${point.y}`);
    }
}
