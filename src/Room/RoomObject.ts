import {Room} from './Room';

/**
 * @class RoomObject
 */
export abstract class RoomObject {
    /**
     * @private
     * @property
     */
    private isDestroyed = false;

    /**
     * @private
     * @property
     */
    private room$: Room;

    /**
     * Destroy room object
     *
     * @public
     */
    public destroy(): void {
        if (this.isDestroyed) {
            return;
        }

        this.isDestroyed = true;

        this.room.removeObject(this);

        this.room$ = undefined as any;
        this.destroyed();
    }

    /**
     * Get room
     *
     * @public
     * @returns {Room}
     */
    public get room(): Room {
        return this.room$;
    }

    /**
     * Set room
     *
     * @public
     */
    public set room(room: Room) {
        if (this.room || this.room !== undefined) {
            throw new Error('RoomObject already provided with a room.');
        }

        this.isDestroyed = false;
        this.room$ = room;

        this.registered();
    }

    abstract destroyed(): void;
    abstract registered(): void;
}
