import {injectable} from 'tsyringe';
import {Container, Renderer} from 'pixi.js';
import {Game} from '../Game';
import {Room} from './Room';

@injectable()
/**
 * @class RoomVisualization
 * @extends Container
 */
export class RoomVisualization extends Container {
    /**
     * @public
     * @property
     */
    public container: Container = new Container();

    /**
     * @public
     * @property
     */
    public baseContainer: Container = new Container();

    /**
     * @public
     * @property
     */
    public behindWallContainer: Container = new Container();

    /**
     * @public
     * @property
     */
    public landscapeContainer: Container = new Container();

    /**
     * @public
     * @property
     */
    public floorContainer: Container = new Container();

    /**
     * @public
     * @property
     */
    public wallContainer: Container = new Container();

    /**
     * @public
     * @property
     */
    public renderer: Renderer;

    /**
     * @private
     * @property
     */
    private room$: Room;

    /**
     * RoomVisualization constructor
     *
     * @param game
     */
    constructor(private game: Game) {
        super();

        this.renderer = this.game.application.renderer;

        this.container.sortableChildren = true;
        this.behindWallContainer.sortableChildren = true;

        this.baseContainer.addChild(this.wallContainer);
        this.baseContainer.addChild(this.floorContainer);

        this.baseContainer.sortableChildren = true;
        this.baseContainer.cacheAsBitmap = true;

        this.addChild(this.behindWallContainer);
        this.addChild(this.baseContainer);
        this.addChild(this.landscapeContainer);
        this.addChild(this.container);
    }

    /**
     * Disable caching
     *
     * @public
     * @returns {boolean}
     */
    public disableCache(): boolean {
        return (this.baseContainer.cacheAsBitmap = false);
    }

    /**
     * Enable caching
     *
     * @public
     * @returns {boolean}
     */
    public enableCache(): boolean {
        return (this.baseContainer.cacheAsBitmap = true);
    }

    /**
     * Get room value
     *
     * @returns {Room}
     */
    public get room(): Room {
        return this.room$;
    }

    /**
     * Set new room value
     *
     * @param room
     */
    public set room(room: Room) {
        this.room$ = room; // was dat
        /* this.masks.forEach((mask) => mask.updateRoom(room));
        this._updateRoomVisualizationMeta({
          masks: this.masks,
          wallHeight: this.room.wallHeight,
          wallHeightWithZ: this.room.wallHeightWithZ,
        }); */
    }
}
