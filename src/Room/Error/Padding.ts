/**
 * @class Padding
 * @extends Error
 */
export class Padding extends Error {
    /**
     * Padding constructor
     */
    constructor(message: string) {
        super(
            `
            No padding for ${message}
            - There should be one 'x' at the beginning of each row
            - There should be a full row of 'x' as the first row
            Please ensure that the tilemap is padded like the following example:
            xxx
            xoo
            xoo
            `
        );

        this.name = 'PaddingError';
    }
}
