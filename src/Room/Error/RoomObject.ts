/**
 * @class RoomObjectError
 * @extends Error
 */
export class RoomObjectError extends Error {
    /**
     * RoomObjectError constructor
     *
     * @param message
     */
    constructor(message: string) {
        super(`[RoomObjectError] ${message}`);

        this.name = 'RoomObjectError';
    }
}
