import {injectable} from 'tsyringe';
import {Container, Point, Texture} from 'pixi.js';
import {RoomObject} from './RoomObject';
import {RoomVisualization} from './RoomVisualization';
import {MapTileType} from './Type/Tile';
import {RoomMap} from './RoomMap';
import {DoorType} from './Interface/DoorType';
import {HiddenType} from './Interface/HiddenType';
import {StairType} from './Interface/StairType';
import {TileType} from './Interface/TileType';
import {WallType} from './Interface/WallType';
import {Tile} from './Object/Tile';
import {Stair} from './Object/Stair';
import {Wall} from './Object/Wall';
import {Game} from '../Game';
import {TileCursor} from './Object/TileCursor';
import {RoomPosition} from './Interface/RoomPosition';
import {BehaviorSubject} from 'rxjs';
import {Camera} from './Camera';

@injectable()
/**
 * @class Room
 * @extends Container
 */
export class Room extends Container {
    /**
     * @private
     * @property
     */
    private objects$: Set<RoomObject> = new Set<RoomObject>();

    /**
     * @private
     * @property
     */
    private mapBounds: {
        minX: number;
        minY: number;
        maxX: number;
        maxY: number;
    };

    /**
     * @private
     * @property
     */
    private wallOffsets: Point = new Point(1, 1);

    /**
     * @private
     * @property
     */
    private positionOffsets: Point = new Point(1, 1);

    /**
     * @private
     * @property
     */
    private maskOffsets: Point = new Point(0, 0);

    /**
     * @private
     * @property
     */
    private largestDiff: number;

    /**
     * @private
     * @property
     */
    private map: (DoorType | HiddenType | StairType | TileType | WallType)[][];

    /**
     * @private
     * @property
     */
    private wallDepth$ = 8;

    /**
     * @private
     * @property
     */
    private wallHeight$ = 116;

    /**
     * @private
     * @property
     */
    private tileHeight$ = 8;

    /**
     * @private
     * @property
     */
    private walls: Wall[] = [];

    /**
     * @private
     * @property
     */
    private floor: (Stair | Tile)[] = [];

    /**
     * @private
     * @property
     */
    private doorWall: Wall | undefined;

    /**
     * @private
     * @property
     */
    private cursors: TileCursor[] = [];

    /**
     * @private
     * @property
     */
    private floorColor$: string | undefined;

    /**
     * @private
     * @property
     */
    private wallColor$: string | undefined;

    /**
     * @private
     * @property
     */
    private tileColor = '#989865';

    /**
     * @private
     * @property
     */
    private currentWallTexture: Texture | undefined;

    /**
     * @private
     * @property
     */
    private currentFloorTexture: Texture | undefined;

    /**
     * @private
     * @property
     */
    private wallTexture$: Promise<Texture> | Texture | undefined;

    /**
     * @private
     * @property
     */
    private floorTexture$: Promise<Texture> | Texture;

    /**
     * @private
     * @property
     */
    private hideWalls$ = false;

    /**
     * @private
     * @property
     */
    private hideFloor$ = false;

    private activeTileSubject: BehaviorSubject<
        RoomPosition | undefined
    > = new BehaviorSubject<RoomPosition | undefined>(undefined);

    /**
     * Room constructor
     *
     * @param visualization
     * @param roomMap
     * @param game
     */
    constructor(
        public visualization: RoomVisualization,
        public roomMap: RoomMap,
        private game: Game,
        private camera: Camera
    ) {
        super();
    }

    /**
     * @public
     * @param map
     */
    public init(tileMap: string | MapTileType[][]): void {
        const normalizedTileMap =
            typeof tileMap === 'string'
                ? this.roomMap.parseMapFromString(tileMap)
                : tileMap;

        const {
            largestDiff,
            map,
            wallOffsets,
            positionOffsets,
            maskOffsets,
        } = this.roomMap.parseMap(normalizedTileMap);

        this.wallOffsets = wallOffsets;
        this.positionOffsets = positionOffsets;
        this.maskOffsets = maskOffsets;

        this.largestDiff = largestDiff;

        this.map = map;

        this.mapBounds = this.roomMap.getMapBounds(map, this.wallOffsets);

        this.floorTexture = this.game.loadTexture('tile.png');

        this.visualization.room = this;
        this.updatePosition();
        this.updateTiles();
        this.addChild(this.visualization);

        this.camera.room = this;
        this.camera.init();

        this.x = this.game.application.screen.width / 2 - this.roomWidth / 2;
        this.y = this.game.application.screen.height / 2 - this.roomHeight / 2;

        this.game.application.stage.addChild(this.camera);
    }

    /**
     *
     * @param roomX
     * @param roomY
     * @param roomZ
     */
    public getPosition(roomX: number, roomY: number, roomZ: number): Point {
        const point = new Point(roomX, roomY);

        const base = 32;

        // TODO: Right now we are subtracting the tileMapBounds here.
        // This is so the landscapes work correctly. This has something with the mask position being negative for some walls.
        // This fixes it for now.
        const xPos = -this.mapBounds.minX + point.x * base - point.y * base;
        const yPos = point.x * (base / 2) + point.y * (base / 2);

        return new Point(xPos, yPos - roomZ * 32);
    }

    getTileAt(
        point: Point
    ): DoorType | HiddenType | StairType | TileType | WallType | undefined {
        point = point.set(
            point.x + this.positionOffsets.x,
            point.y + this.positionOffsets.y
        );

        const row = this.map[point.y];

        if (row === undefined) {
            return;
        }

        if (row[point.y] === undefined) {
            return;
        }

        return row[point.y];
    }

    /**
     * @public
     */
    public get roomBounds(): {
        minX: number;
        minY: number;
        maxX: number;
        maxY: number;
    } {
        return {
            ...this.mapBounds,
            minX: this.mapBounds.minX - this.wallDepth,
            maxX: this.mapBounds.maxX + this.wallDepth,
            minY: this.mapBounds.minY - this.wallHeight - this.wallDepth,
            maxY: this.mapBounds.maxY + this.tileHeight,
        };
    }

    /**
     * @public
     * @returns {number}
     */
    public get wallDepth(): number {
        return this.wallDepth$;
    }

    /**
     * @public
     */
    public set wallDepth(value: number) {
        this.wallDepth$ = value;
        this.updateWallDepth();
    }

    /**
     * Height of the room.
     */
    public get roomHeight(): number {
        return this.roomBounds.maxY - this.roomBounds.minY;
    }

    /**
     * Width of the room.
     */
    public get roomWidth(): number {
        return this.roomBounds.maxX - this.roomBounds.minX;
    }

    /**
     * @public
     * @returns {number}
     */
    public get tileHeight(): number {
        return this.tileHeight$;
    }

    /**
     * @public
     */
    public set tileHeight(value: number) {
        this.tileHeight$ = value;
        this.updateTileHeight();
    }

    /**
     * @public
     * @returns {number}
     */
    public get wallHeight(): number {
        return this.wallHeight$;
    }

    /**
     * @public
     */
    public set wallHeight(value: number) {
        this.wallHeight$ = value;
        this.updateWallHeight();
    }

    /**
     * @public
     * @returns {number}
     */
    public get wallHeightWithZ(): number {
        return this.wallHeight + this.largestDiff * 32;
    }

    /**
     * Add room object
     *
     * @public
     * @param object
     */
    public addObject(object: RoomObject): void {
        if (this.objects$.has(object)) {
            return;
        }

        object.room = this;
        this.objects$.add(object);
    }

    /**
     * Remove room object
     *
     * @public
     * @param object
     */
    public removeObject(object: RoomObject): void {
        if (!this.objects$.has(object)) {
            return;
        }

        object.destroy();
        this.objects$.delete(object);
    }

    /**
     * Get room objects
     *
     * @public
     * @returns {ReadonlySet<RoomObject>}
     */
    public get objects(): ReadonlySet<RoomObject> {
        return this.objects$;
    }

    /**
     * @public
     * @returns {string | undefined}
     */
    public get wallColor(): string | undefined {
        return this.wallColor$;
    }

    /**
     * @public
     */
    public set wallColor(value: string | undefined) {
        this.wallColor$ = value;
        this.updateTextures();
    }

    /**
     * @public
     * @returns {Promise<Texture> | Texture | undefined}
     */
    public get wallTexture(): Promise<Texture> | Texture | undefined {
        return this.wallTexture$;
    }

    /**
     * @public
     */
    public set wallTexture(value: Promise<Texture> | Texture | undefined) {
        this.wallTexture$ = value;
        this.loadWallTextures();
    }

    /**
     * @public
     * @returns {Promise<Texture> | Texture | undefined}
     */
    public get floorTexture(): Promise<Texture> | Texture {
        return this.floorTexture$;
    }

    /**
     * @public
     */
    public set floorTexture(value: Promise<Texture> | Texture) {
        this.floorTexture$ = value;
        this.loadFloorTextures();
    }

    /**
     * @public
     * @returns {boolean}
     */
    public get hideWalls(): boolean {
        return this.hideWalls$;
    }

    public set hideWalls(value: boolean) {
        this.hideWalls$ = value;
        this.updateTiles();
    }

    /**
     * @public
     * @returns {boolean}
     */
    public get hideFloor(): boolean {
        return this.hideFloor$;
    }

    /**
     * @public
     */
    public set hideFloor(value: boolean) {
        this.hideFloor$ = value;
        this.updateTiles();
    }

    /**
     * @public
     * @returns {string | undefined}
     */
    public get floorColor(): string | undefined {
        return this.floorColor$;
    }

    /**
     * @public
     */
    public set floorColor(value: string | undefined) {
        this.floorColor$ = value;
        this.updateTextures();
    }

    /**
     * @private
     */
    private updatePosition(): void {
        this.visualization.x =
            Math.round(-this.roomBounds.minX / 2) * 2 + this.mapBounds.minX;
        this.visualization.y = Math.round(-this.roomBounds.minY / 2) * 2;
    }

    /**
     * @private
     */
    private updateTileHeight(): void {
        this.updatePosition();
        this.visualization.disableCache();

        this.floor.forEach((floor: Stair | Tile) => {
            floor.tileHeight = this.tileHeight;
        });

        this.walls.forEach((wall: Wall) => {
            wall.tileHeight = this.tileHeight;
        });

        this.visualization.enableCache();
    }

    /**
     * @private
     */
    private updateWallHeight(): void {
        this.updatePosition();
        this.visualization.room = this;
        this.visualization.disableCache();
        this.walls.forEach((wall: Wall) => {
            wall.wallHeight = this.wallHeightWithZ;
        });
        this.visualization.enableCache();
    }

    /**
     * @private
     */
    private updateWallDepth(): void {
        this.updatePosition();
        this.visualization.disableCache();
        this.walls.forEach((wall: Wall) => {
            wall.wallDepth = this.wallDepth;
        });
        this.visualization.enableCache();
    }

    private updateTiles() {
        this.resetTiles();

        const tiles = this.map;

        for (let y = 0; y < tiles.length; y++) {
            for (let x = 0; x < tiles[y].length; x++) {
                const tile = tiles[y][x];

                if (tile.type === 'door') {
                    this.registerTile(
                        new Tile({
                            room: this,
                            x,
                            y,
                            z: tile.z,
                            edge: true,
                            tileHeight: 0,
                            color: this.floorColor ?? this.tileColor,
                            door: true,
                        })
                    );

                    const wall = new Wall({
                        room: this,
                        x,
                        y,
                        direction: 'left',
                        tileHeight: this.tileHeight,
                        wallHeight: this.wallHeightWithZ,
                        z: tile.z,
                        color: this.getWallColor(),
                        texture: this.currentWallTexture,
                        wallDepth: this.wallDepth,
                        hideBorder: true,
                        doorHeight: 30,
                    });

                    this.registerWall(wall);

                    this.doorWall = wall;

                    this.registerTileCursor(
                        {
                            roomX: x,
                            roomY: y,
                            roomZ: tile.z,
                        },
                        true
                    );
                }

                if (tile.type === 'tile') {
                    this.registerTile(
                        new Tile({
                            room: this,
                            x,
                            y,
                            z: tile.z,
                            edge: true,
                            tileHeight: this.tileHeight,
                            color: this.floorColor ?? this.tileColor,
                        })
                    );

                    this.registerTileCursor({
                        roomX: x,
                        roomY: y,
                        roomZ: tile.z,
                    });
                }

                const direction = this.getWallDirection(tile);

                if (direction !== undefined && tile.type === 'wall') {
                    this.registerWall(
                        new Wall({
                            room: this,
                            x,
                            y,
                            direction,
                            tileHeight: this.tileHeight,
                            wallHeight: this.wallHeightWithZ,
                            z: tile.height,
                            color: this.getWallColor(),
                            texture: this.currentWallTexture,
                            wallDepth: this.wallDepth,
                            hideBorder: tile.hideBorder,
                        })
                    );
                }

                if (tile.type === 'wall' && tile.kind === 'innerCorner') {
                    this.registerWall(
                        new Wall({
                            room: this,
                            x,
                            y,
                            direction: 'right',
                            tileHeight: this.tileHeight,
                            wallHeight: this.wallHeightWithZ,
                            side: false,
                            z: tile.height,
                            color: this.getWallColor(),
                            wallDepth: this.wallDepth,
                        })
                    );

                    this.registerWall(
                        new Wall({
                            room: this,
                            x,
                            y,
                            direction: 'left',
                            tileHeight: this.tileHeight,
                            wallHeight: this.wallHeightWithZ,
                            side: false,
                            z: tile.height,
                            color: this.getWallColor(),
                            wallDepth: this.wallDepth,
                        })
                    );
                }

                if (tile.type === 'stairs') {
                    this.registerTile(
                        new Stair({
                            room: this,
                            x,
                            y,
                            z: tile.z,
                            tileHeight: this.tileHeight,
                            color: this.tileColor,
                            direction: tile.kind,
                        })
                    );

                    this.registerTileCursor({
                        roomX: x,
                        roomY: y,
                        roomZ: tile.z,
                    });

                    this.registerTileCursor({
                        roomX: x,
                        roomY: y,
                        roomZ: tile.z + 1,
                    });
                }
            }
        }
    }

    /**
     * @private
     * @returns {string}
     */
    private getWallColor(): string {
        if (
            this.wallColor === undefined &&
            this.currentWallTexture !== undefined
        ) {
            return '#ffffff';
        }

        if (
            this.wallColor === undefined &&
            this.currentWallTexture === undefined
        ) {
            return '#b6b8c7';
        }

        return '#ffffff';
    }

    /**
     * @private
     */
    private resetTiles(): void {
        [...this.floor, ...this.walls, ...this.cursors].forEach((value) =>
            value.destroy()
        );

        this.floor = [];
        this.walls = [];
        this.cursors = [];
        this.doorWall = undefined;
    }

    /**
     * @private
     */
    private loadWallTextures(): void {
        Promise.resolve(this.wallTexture)
            .then((texture) => {
                this.currentWallTexture = texture;
                this.updateTextures();
            })
            .catch((reason: string) => {
                throw new Error(`Couldn't load wall textures: ${reason}`);
            });
    }

    /**
     * @private
     */
    private loadFloorTextures(): void {
        Promise.resolve(this.floorTexture)
            .then((texture) => {
                this.currentFloorTexture = texture;
                this.updateTextures();
            })
            .catch((reason: string) => {
                throw new Error(`Couldn't load floor textures: ${reason}`);
            });
    }

    private getWallDirection(
        tile: DoorType | HiddenType | StairType | TileType | WallType
    ): 'left' | 'right' | 'corner' | undefined {
        if (tile.type !== 'wall') {
            return;
        }

        if (tile.kind === 'rowWall') {
            return 'left';
        }
        if (tile.kind === 'colWall') {
            return 'right';
        }
        if (tile.kind === 'outerCorner') {
            return 'corner';
        }
    }

    /**
     * @private
     */
    private updateTextures(): void {
        this.visualization.disableCache();
        this.updateTiles();

        this.walls.forEach((wall: Wall) => {
            wall.texture = this.currentWallTexture;
            wall.color = this.wallColor;
        });

        this.floor.forEach((floor: Stair | Tile) => {
            floor.texture = this.currentFloorTexture;
            floor.color = this.floorColor;
        });

        this.visualization.enableCache();
    }

    /**
     * @private
     * @param wall
     */
    private registerWall(wall: Wall): void {
        if (this.hideWalls || this.hideFloor) {
            return;
        }

        this.walls.push(wall);
        this.addObject(wall);
    }

    /**
     * @private
     * @param tile
     */
    private registerTile(tile: Stair | Tile): void {
        if (this.hideFloor) {
            return;
        }

        this.floor.push(tile);
        this.addObject(tile);
    }

    /**
     * @private
     * @param position
     * @param door
     */
    private registerTileCursor(position: RoomPosition, door?: boolean): void {
        const cursor = new TileCursor(position, door);

        cursor.onClick(position, (pos) => {});
        /*cursor.onOver((pos: RoomPosition) => {
            this.activeTileSubject.next(position);
            console.log('on over:', pos);
        });
        cursor.onOut(position, (pos) => {
            this.activeTileSubject.next(undefined);
            console.log('on out: ', pos);
        });*/

        this.cursors.push(cursor);
        this.addObject(cursor);
    }
}
