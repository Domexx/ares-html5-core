import {Texture} from 'pixi.js';
import {Room} from '../Room';

/**
 * @interface WallProps
 */
export interface WallProps {
    room: Room;
    z: number;
    x: number;
    y: number;
    direction: 'left' | 'right' | 'corner';
    tileHeight: number;
    wallHeight: number;
    wallDepth: number;
    side?: boolean;
    color: string;
    texture?: Texture;
    hideBorder?: boolean;
    doorHeight?: number;
}
