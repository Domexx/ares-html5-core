/**
 * @interface WallType
 */
export interface WallType {
    type: 'wall';
    kind: 'colWall' | 'rowWall' | 'innerCorner' | 'outerCorner';
    height: number;
    hideBorder?: boolean;
}
