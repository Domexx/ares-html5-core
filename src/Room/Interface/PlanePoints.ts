import {Point} from 'pixi.js';

/**
 * @interface PlanePoints
 */
export interface PlanePoints {
    a: Point;
    b: Point;
    c: Point;
    d: Point;
}
