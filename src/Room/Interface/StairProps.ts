import {Point, Texture} from 'pixi.js';
import {Room} from '../Room';

/**
 * @interface StairPros
 */
export interface StairProps {
    room: Room;
    z: number;
    x: number;
    y: number;
    tileHeight: number;
    color: string;
    direction: number | 0 | 2;
    texture?: Texture;
}

/**
 * @interface StairBoxProps
 */
export interface StairBoxProps {
    x: number;
    y: number;
    index: number;
    tileHeight: number;
    color: string;
    tilePositions: {left: Point; right: Point; top: Point};
}
