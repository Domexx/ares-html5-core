/**
 * @interface HiddenType
 */
export interface HiddenType {
    type: 'hidden';
}
