import {Dimension} from '../../Interface/Dimension';

/**
 * @interface FloorMatrix
 * @extends Dimension
 */
export interface FloorMatrix extends Dimension {
    x: number;
    y: number;
}
