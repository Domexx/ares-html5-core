/**
 * @interface DoorType
 */
export interface DoorType {
    type: 'door';
    z: number;
}
