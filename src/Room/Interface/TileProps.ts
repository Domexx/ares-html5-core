import {Texture} from 'pixi.js';
import {Room} from '../Room';

/**
 * @interface TileProps
 */
export interface TileProps {
    room: Room;
    x: number;
    y: number;
    z: number;
    tileHeight: number;
    color: string;
    texture?: Texture;
    door?: boolean;
    edge?: boolean;
}
