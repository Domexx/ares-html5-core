/**
 * @interface TileInfo
 */
export interface TileInfo {
    rowEdge: boolean;
    colEdge: boolean;
    innerEdge: boolean;
    stairs: {direction: number} | undefined;
    height: number;
    rowDoor: boolean;
}
