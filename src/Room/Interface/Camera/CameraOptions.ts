/**
 * @interface CameraOptions
 */
export interface CameraOptions {
    duration?: number;
    target?: EventTarget;
}
