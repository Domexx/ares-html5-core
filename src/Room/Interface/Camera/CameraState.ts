/**
 * @interface CameraState
 */
export interface CameraState {
    type: 'WAITING';
}

/**
 * @interface CameraDraggingState
 */
export interface CameraDraggingState {
    type: 'DRAGGING';
    currentX: number;
    currentY: number;
    pointerId: number;
    startX: number;
    startY: number;
    skipBoundsCheck?: boolean;
}

/**
 * @interface CameraAnimateZeroState
 */
export interface CameraAnimateZeroState {
    type: 'ANIMATE_ZERO';
    currentX: number;
    currentY: number;
    startX: number;
    startY: number;
}

/**
 * @interface CameraWaitForDistanceState
 */
export interface CameraWaitForDistanceState {
    type: 'WAIT_FOR_DISTANCE';
    startX: number;
    startY: number;
    pointerId: number;
}
