/**
 * @interface TileType
 */
export interface TileType {
    type: 'tile';
    z: number;
}
