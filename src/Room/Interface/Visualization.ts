import { Container, Sprite } from 'pixi.js';
import { VisualizationMeta } from '../Type/VisualizationMeta';

/**
 * @interface Visualization
 */
export interface Visualization {
    container: Container;
    behindWallContainer: Container;
    landscapeContainer: Container;
    floorContainer: Container;
    wallContainer: Container;

    addMask(id: string, element: Sprite): any;

    subscribeRoomMeta(
        listener: (value: VisualizationMeta) => void
    ): {unsubscribe: () => void};
}