/**
 * @interface ColumnWall
 */
export interface ColumnWall {
    startX: number;
    endX: number;
    y: number;
    height: number;
}
