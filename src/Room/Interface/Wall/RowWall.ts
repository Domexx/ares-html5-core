/**
 * @interface RowWall
 */
export interface RowWall {
    startY: number;
    endY: number;
    x: number;
    height: number;
}
