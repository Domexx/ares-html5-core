import {ColumnWall} from './ColumnWall';
import {RowWall} from './RowWall';

/**
 * @interface WallEntry
 */
export interface WallEntry {
    id: string;
    wall: RowWall | ColumnWall;
}
