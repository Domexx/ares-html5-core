/**
 * @interface Tint
 */
export interface Tint {
    topTint: number;
    rightTint: number;
    leftTint: number;
}

/**
 * @interface TileTint
 */
export interface TileTint {
    tileTint: number;
    borderRightTint: number;
    borderLeftTint: number;
}