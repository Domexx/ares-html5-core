/**
 * @interface RoomPosition
 */
export interface RoomPosition {
    roomX: number;
    roomY: number;
    roomZ: number;
}
