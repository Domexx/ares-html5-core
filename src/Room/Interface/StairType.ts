/**
 * @interface StairType
 */
export interface StairType {
    type: 'stairs';
    kind: number | 0 | 2;
    z: number;
}
