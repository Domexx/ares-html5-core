export type PlanePoints = {
    a: {x: number; y: number};
    b: {x: number; y: number};
    c: {x: number; y: number};
    d: {x: number; y: number};
};
