import { RoomLandscapeMaskSprite } from '../Sprite/RoomLandscapeMaskSprite';

/**
 * @type VisualizationMeta
 */
export type VisualizationMeta = {
    masks: Map<string, RoomLandscapeMaskSprite>;
    wallHeight: number;
    wallHeightWithZ: number;
};