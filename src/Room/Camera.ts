import {Container, InteractionEvent, Point, Rectangle} from 'pixi.js';
import {BehaviorSubject, Observable} from 'rxjs';
import {injectable} from 'tsyringe';
import {Easing, Tween, update} from '@tweenjs/tween.js';
import {
    CameraAnimateZeroState,
    CameraDraggingState,
    CameraState,
    CameraWaitForDistanceState,
} from './Interface/Camera/CameraState';
import {Room} from './Room';
import {CameraOptions} from './Interface/Camera/CameraOptions';
import {Game} from '../Game';

/**
 * @class Camera
 */
@injectable()
export class Camera extends Container {
    /**
     * @property
     * @public
     */
    public room: Room;

    /**
     * @property
     * @private
     */
    private stateSubject$: BehaviorSubject<
        | CameraState
        | CameraWaitForDistanceState
        | CameraDraggingState
        | CameraAnimateZeroState
    > = new BehaviorSubject<
        | CameraState
        | CameraWaitForDistanceState
        | CameraDraggingState
        | CameraAnimateZeroState
    >({type: 'WAITING'});

    /**
     * @property
     * @private
     */
    private offsets: Point = new Point(0, 0);

    /**
     * @property
     * @private
     */
    private animatedOffsets: Point = new Point(0, 0);

    /**
     * @property
     * @private
     */
    private container: Container;

    /**
     * @property
     * @private
     */
    private parentContainer: Container;

    /**
     * @property
     * @private
     */
    private parentBounds: () => Rectangle;

    private cameraOptions: CameraOptions | undefined;

    private tween: Tween<{x: number; y: number}>;
    private target: EventTarget;

    /**
     * Camera constructor
     *
     * @param game
     */
    constructor(private game: Game) {
        super();
    }

    /**
     * Initialize room camera
     */
    public init(options?: CameraOptions): void {
        this.parentBounds = () => this.game.application.screen;

        this.cameraOptions = options;

        const target = options?.target ?? window;
        this.target = target;

        this.parentContainer = new Container();
        this.parentContainer.hitArea = this.parentBounds();
        this.parentContainer.interactive = true;

        this.container = new Container();
        this.container.addChild(this.room);
        this.parentContainer.addChild(this.container);

        this.addChild(this.parentContainer);

        // Activation of the camera is only triggered by a down event on the parent container.
        this.parentContainer.addListener(
            'pointerdown',
            this.handlePointerDown.bind(this)
        );
        this.target.addEventListener(
            'pointermove',
            this.handlePointerMove.bind(this) as any
        );
        this.target.addEventListener(
            'pointerup',
            this.handlePointerUp.bind(this) as any
        );

        let last: number | undefined;
        this.game.application.ticker.add(() => {
            if (last == null) {
                last = performance.now();
            }

            const value = performance.now() - last;

            update(value);
        });
    }

    public destroy(): void {
        this.parentContainer.removeListener(
            'pointerdown',
            this.handlePointerDown.bind(this)
        );
        this.target.removeEventListener(
            'pointermove',
            this.handlePointerMove.bind(this) as any
        );
        this.target.removeEventListener(
            'pointerup',
            this.handlePointerUp.bind(this) as any
        );
    }

    /**
     * @public
     * @returns {CameraState | CameraWaitForDistanceState | CameraDraggingState | CameraAnimateZeroState}
     */
    public get state():
        | CameraState
        | CameraWaitForDistanceState
        | CameraDraggingState
        | CameraAnimateZeroState {
        return this.stateSubject$.value;
    }

    /**
     * @public
     * @returns {Observable<CameraState | CameraWaitForDistanceState | CameraDraggingState | CameraAnimateZeroState>}
     */
    public get stateAsObservable(): Observable<
        | CameraState
        | CameraWaitForDistanceState
        | CameraDraggingState
        | CameraAnimateZeroState
    > {
        return this.stateSubject$.asObservable();
    }

    /**
     * Handle pointerup event
     *
     * @private
     * @param event
     */
    private handlePointerUp(event: PointerEvent): void {
        if (
            this.state.type === 'WAITING' ||
            this.state.type === 'ANIMATE_ZERO'
        ) {
            return;
        }

        if (this.state.pointerId !== event.pointerId) {
            return;
        }

        let animatingBack = false;

        if (this.state.type === 'DRAGGING') {
            animatingBack = this.stopDragging(this.state);
        }

        if (!animatingBack) {
            this.resetDrag();
        }
    }

    /**
     * Handle pointerdown event
     *
     * @private
     * @param event
     */
    private handlePointerDown(event: InteractionEvent): void {
        const position = event.data.getLocalPosition(this.parent);
        if (this.state.type === 'WAITING') {
            this.enterWaitingForDistance(position, event.data.pointerId);
        } else if (this.state.type === 'ANIMATE_ZERO') {
            this.changingDragWhileAnimating(position, event.data.pointerId);
        }
    }

    /**
     * Handle pointermove event
     *
     * @private
     * @param event
     */
    private handlePointerMove(event: PointerEvent): void {
        const box = this.game.application.view.getBoundingClientRect();
        const position = new Point(
            event.clientX - box.x - this.parent.worldTransform.tx,
            event.clientY - box.y - this.parent.worldTransform.tx
        );

        switch (this.state.type) {
            case 'WAIT_FOR_DISTANCE': {
                this.tryUpgradeWaitForDistance(
                    this.state,
                    position,
                    event.pointerId
                );
                break;
            }

            case 'DRAGGING': {
                this.updateDragging(this.state, position, event.pointerId);
                break;
            }
        }
    }

    /**
     * @private
     */
    private updatePosition(): void {
        switch (this.state.type) {
            case 'DRAGGING': {
                // When dragging, the current position consists of the current offset of the camera
                // and the drag difference.

                const diffX = this.state.currentX - this.state.startX;
                const diffY = this.state.currentY - this.state.startY;

                this.container.x = this.offsets.x + diffX;
                this.container.y = this.offsets.y + diffY;
                break;
            }

            case 'ANIMATE_ZERO': {
                // When animating back to the zero point, we use the animatedOffsets of the camera.

                this.container.x = this.animatedOffsets.x;
                this.container.y = this.animatedOffsets.y;
                break;
            }

            default: {
                // Default behavior: Use the set offsets of the camera.

                this.container.x = this.offsets.x;
                this.container.y = this.offsets.y;
            }
        }
    }

    /**
     * @private
     * @param offsets
     * @returns {boolean}
     */
    private isOutOfBounds(offsets: {x: number; y: number}): boolean {
        const roomX = this.parent.transform.position.x + this.room.x;
        const roomY = this.parent.transform.position.y + this.room.y;

        if (roomX + this.room.roomWidth + offsets.x <= 0) {
            // The room is out of bounds to the left side.
            return true;
        }

        if (roomX + offsets.x >= this.parentBounds().width) {
            // The room is out of bounds to the right side.
            return true;
        }

        if (roomY + this.room.roomHeight + offsets.y <= 0) {
            // The room is out of bounds to the top side.
            return true;
        }

        if (roomY + offsets.y >= this.parentBounds().height) {
            // The room is out of bounds to the botoom side.
            return true;
        }

        return false;
    }

    /**
     * @private
     * @param state
     * @param current
     */
    private returnToZero(state: CameraDraggingState, current: Point): void {
        this.stateSubject$.next({
            ...state,
            type: 'ANIMATE_ZERO',
        });
        const duration = this.cameraOptions?.duration ?? 500;

        this.animatedOffsets = current;
        this.offsets.set(0, 0);

        const newPos = {...this.animatedOffsets};

        const tween = new Tween(newPos)
            .to({x: 0, y: 0}, duration)
            .easing(Easing.Quadratic.Out) // Use an easing function to make the animation smooth.
            .onUpdate((pos: {x: number; y: number}, value: number) => {
                this.animatedOffsets.set(newPos.x, newPos.y);

                if (value >= 1) {
                    this.stateSubject$.next({type: 'WAITING'});
                }

                this.updatePosition();
            })
            .start();

        this.tween = tween;

        this.updatePosition();
    }

    /**
     * @private
     * @returns {boolean}
     */
    private stopDragging(state: CameraDraggingState): boolean {
        const diffX = state.currentX - state.startX;
        const diffY = state.currentY - state.startY;

        const currentOffsets = new Point(
            this.offsets.x + diffX,
            this.offsets.y + diffY
        );

        if (
            this.isOutOfBounds(currentOffsets) ||
            (state.skipBoundsCheck != null && state.skipBoundsCheck)
        ) {
            this.returnToZero(state, currentOffsets);
            return true;
        } else {
            this.offsets = currentOffsets;
        }

        return false;
    }

    /**
     * @private
     */
    private resetDrag(): void {
        this.stateSubject$.next({type: 'WAITING'});
        this.updatePosition();
    }

    /**
     * @private
     * @param position
     * @param pointerId
     */
    private changingDragWhileAnimating(
        position: Point,
        pointerId: number
    ): void {
        this.offsets = this.animatedOffsets;
        this.animatedOffsets.set(0, 0);
        this.tween.stop();

        this.stateSubject$.next({
            currentX: position.x,
            currentY: position.y,
            startX: position.x,
            startY: position.y,
            pointerId,
            type: 'DRAGGING',
            skipBoundsCheck: true,
        });

        this.updatePosition();
    }

    /**
     * @private
     * @param position
     * @param pointerId
     */
    private enterWaitingForDistance(position: Point, pointerId: number): void {
        this.stateSubject$.next({
            type: 'WAIT_FOR_DISTANCE',
            pointerId,
            startX: position.x,
            startY: position.y,
        });
    }

    /**
     * @private
     * @param state
     * @param position
     * @param pointerId
     */
    private tryUpgradeWaitForDistance(
        state: CameraWaitForDistanceState,
        position: Point,
        pointerId: number
    ): void {
        if (state.pointerId !== pointerId) {
            return;
        }

        const distance = Math.sqrt(
            (position.x - state.startX) ** 2 + (position.y - state.startY) ** 2
        );

        // When the distance of the pointer travelled more than 10px, start dragging.
        if (distance >= 10) {
            this.stateSubject$.next({
                currentX: position.x,
                currentY: position.y,
                startX: position.x,
                startY: position.y,
                pointerId,
                type: 'DRAGGING',
            });
            this.updatePosition();
        }
    }

    /**
     * @private
     * @param state
     * @param position
     * @param pointerId
     */
    private updateDragging(
        state: CameraDraggingState,
        position: Point,
        pointerId: number
    ): void {
        if (state.pointerId !== pointerId) {
            return;
        }

        this.stateSubject$.next({
            ...state,
            currentX: position.x,
            currentY: position.y,
        });

        this.updatePosition();
    }
}
