import {Matrix, Point} from 'pixi.js';
import {injectable} from 'tsyringe';
import {Dimension} from '../Interface/Dimension';
import {FloorMatrix} from './Interface/FloorMatrix';
import {PlanePoints} from './Interface/PlanePoints';

@injectable()
/**
 * @class RoomMatrix
 */
export class RoomMatrix {
    /**
     * @public
     * @param point
     * @returns {Matrix}
     */
    public getFloorMatrix(point: Point): Matrix {
        return this.createPlaneMatrix(
            {
                c: new Point(0, 16),
                d: new Point(32, 0),
                a: new Point(64, 16),
                b: new Point(32, 32),
            },
            {
                width: 32,
                height: 32,
                x: point.x,
                y: point.y,
            }
        );
    }

    /**
     * @public
     * @param point
     * @param dim
     * @returns {Matrix}
     */
    public getLeftMatrix(point: Point, dim: Dimension): Matrix {
        return this.createPlaneMatrix(
            {
                b: new Point(0, 16),
                c: new Point(dim.width, 16 + dim.width / 2),
                d: new Point(dim.width, 16 + dim.width / 2 + dim.height),
                a: new Point(0, 16 + dim.height),
            },
            {
                width: dim.width,
                height: dim.height,
                x: point.x,
                y: point.y,
            }
        );
    }

    /**
     * @public
     * @param point
     * @param dim
     * @returns {Matrix}
     */
    public getRightMatrix(point: Point, dim: Dimension): Matrix {
        return this.createPlaneMatrix(
            {
                b: new Point(32, 32),
                c: new Point(32 + dim.width, 32 - dim.width / 2),
                d: new Point(32 + dim.width, 32 + dim.height - dim.width / 2),
                a: new Point(32, 32 + dim.height),
            },
            {
                width: dim.width,
                height: dim.height,
                x: point.x,
                y: point.y,
            }
        );
    }

    /**
     * @private
     * @param points
     * @param plane
     * @returns {Matrix}
     */
    private createPlaneMatrix(points: PlanePoints, plane: FloorMatrix): Matrix {
        let local3 = points.d.x - points.c.x;
        let local4 = points.d.y - points.c.y;
        let local5 = points.b.x - points.c.x;
        let local6 = points.b.y - points.c.y;

        if (Math.abs(local5 - plane.width) <= 1) {
            local5 = plane.width;
        }

        if (Math.abs(local6 - plane.width) <= 1) {
            local6 = plane.width;
        }

        if (Math.abs(local3 - plane.height) <= 1) {
            local3 = plane.height;
        }

        if (Math.abs(local4 - plane.height) <= 1) {
            local4 = plane.height;
        }

        const base = new Point(plane.x + points.c.x, plane.y + points.c.y);

        return new Matrix(
            local5 / plane.width,
            local6 / plane.width,
            local3 / plane.height,
            local4 / plane.height,
            base.x,
            base.y
        );
    }
}
