import {injectable} from 'tsyringe';
import {TileTint, Tint} from './Interface/Tint';

@injectable()
/**
 * @class RoomPainting
 */
export class RoomPainting {
    /**
     * Gets the Tile colors
     *
     * @param color
     *
     * @returns {TileTint}
     */
    public getTileColors(color: string): TileTint {
        const tileTint = this.fromHex(color);
        const borderLeftTint = this.fromHex(this.adjust(color, -20));
        const borderRightTint = this.fromHex(this.adjust(color, -40));

        return {
            tileTint,
            borderLeftTint,
            borderRightTint,
        };
    }

    /**
     * Gets the Wall Colors
     *
     * @param color
     *
     * @returns {Tint}
     */
    public getWallColors(color: string): Tint {
        const leftTint = this.fromHex(color);
        const topTint = this.fromHex(this.adjust(color, -103));
        const rightTint = this.fromHex(this.adjust(color, -52));

        return {
            topTint,
            leftTint,
            rightTint,
        };
    }

    /**
     * Parses Int out of Hex
     *
     * @param color
     *
     * @returns {number}
     */
    public fromHex(color: string): number {
        return parseInt(color.replace('#', '0x'), 16);
    }

    /**
     * Adjusts the Color
     *
     * @param color
     * @param amount
     *
     * @returns {string}
     */
    public adjust(color: string, amount: number): string {
        return (
            '#' +
            color
                .replace(/^#/, '')
                .replace(/../g, (newColor: string) =>
                    (
                        '0' +
                        Math.min(
                            255,
                            Math.max(0, parseInt(newColor, 16) + amount)
                        ).toString(16)
                    ).substr(-2)
                )
        );
    }
}