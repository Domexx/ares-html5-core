import {filters, Sprite, Renderer, RenderTexture, Container} from 'pixi.js';
import {injectable} from 'tsyringe';
import {Game} from '../../Game';
import {Room} from '../Room';

const negativeFilter = new filters.ColorMatrixFilter();
negativeFilter.negative(false);

/**
 * This class enables us to create a mask which
 * consists of multiple different sprites.
 *
 * This is important for correctly displaying
 * window furniture with landscapes.
 *
 * Windows usually provide a black mask image. This mask image is used
 * to only display the landscape in the area of the mask image.
 *
 * Since there can be multiple windows, and because of that multiple masks,
 * we need a sprite which is able to combine multiple sprites into a single
 * sprite.
 *
 * This Sprite renders its sub-sprites through `RenderTexture`
 * into a single texture, and uses that as a texture for itself.
 */
@injectable()
export class RoomLandscapeMaskSprite extends Sprite {
    private sprites: Set<Sprite> = new Set();
    private roomWidth: number;
    private roomHeight: number;
    private wallHeight: number;
    private renderer: Renderer;

    constructor(private game: Game) {
        super();

        this.renderer = this.game.application.renderer;
    }

    public inititalize({
        width,
        height,
        wallHeight,
        renderer,
    }: {
        width: number;
        height: number;
        wallHeight: number;
        renderer: Renderer;
    }): void {
        this.roomWidth = width;
        this.roomHeight = height;
        this.wallHeight = wallHeight;
        this.renderer = renderer;
    }

    public updateRoom(room: Room): void {
        this.roomHeight = room.roomHeight;
        this.roomWidth = room.roomWidth;
        this.wallHeight = room.wallHeight;
    }

    public add(element: Sprite): void {
        this.sprites.add(element);
        this.updateTexture();
    }

    public remove(element: Sprite): void {
        this.sprites.delete(element);
        this.updateTexture();
    }

    private updateTexture(): void {
        const texture = RenderTexture.create({
            width: this.roomWidth,
            height: this.roomHeight + this.wallHeight,
        });

        const container = new Container();
        this.sprites.forEach((sprite) => {
            // We apply a negative filter to the mask sprite, because the mask assets
            // of the furniture are usually completly black. `js` requires white
            // images to mask an image.
            sprite.filters = [negativeFilter];
            container.addChild(sprite);
        });

        container.y = this.wallHeight;
        this.y = -this.wallHeight;

        this.renderer.render(container, texture);

        this.texture = texture;
    }
}
