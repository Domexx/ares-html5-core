import { Texture, SCALE_MODES } from 'pixi.js';

/**
 * @class HitTexture
 */
export class HitTexture {
    /**
     * @private
     * @property
     */
    private texture: Texture;

    /**
     * @private
     * @property
     */
    private image: HTMLImageElement;

    /**
     * @private
     * @property
     */
    private cachedHitmap: Uint32Array | undefined;

    /**
     * @constructor
     * @param image Image
     */
    constructor(image: HTMLImageElement) {
        if (!image.complete) {
            throw new Error('Image not loaded.');
        }

        this.image = image;
        this.texture = Texture.from(image);
        this.applyTextureProperties(this.texture);
    }

    /**
     * Parses Image from an URL
     *
     * @param imageUrl Image Url
     * @returns {Promise<HitTexture>}
     */
    public async fromUrl(imageUrl: string): Promise<HitTexture> {
        const image = new Image();

        image.crossOrigin = 'anonymous';
        image.src = imageUrl;

        await new Promise<{
            width: number;
            height: number;
        }>((resolve, reject) => {
            image.onload = () => {
                resolve({ width: image.width, height: image.height });
            };

            image.onerror = (value) => reject(value);
        });

        return new HitTexture(image);
    }

    /**
     * Detects the Hit
     *
     * @param x X
     * @param y Y
     * @param transform Transform
     * @param options Options
     */
    public hits(
        x: number,
        y: number,
        transform: { x: number; y: number },
        options: { mirrorHorizonally?: boolean } = { mirrorHorizonally: false }
    ): any {
        if (options.mirrorHorizonally) {
            x = -(x - transform.x);
          } else {
            x = x - transform.x;
          }
          y = y - transform.y;

        const baseTexture = this.texture.baseTexture;
        const hitmap = this.getHitMap();

        const dx = Math.round(x * baseTexture.resolution);
        const dy = Math.round(y * baseTexture.resolution);
        const ind = dx + dy * baseTexture.realWidth;
        const ind1 = ind % 32;
        // eslint-disable-next-line no-bitwise
        const ind2 = (ind / 32) | 0;

        // eslint-disable-next-line no-bitwise
        return (hitmap[ind2] & (1 << ind1)) !== 0;
    }

    /**
     * Applies the Texture Properties
     *
     * @param texture Texture
     * @returns {void}
     */
    public applyTextureProperties(texture: Texture): void {
        texture.baseTexture.scaleMode =  SCALE_MODES.NEAREST;
    }

    /**
     * Gets the Hitmap
     *
     * @private
     *
     * @returns {Uint32Array}
     */
    private getHitMap(): Uint32Array {
        if (this.cachedHitmap == null) {
            this.cachedHitmap = this.generateHitMap(this.image);
          }

          return this.cachedHitmap;
    }

    /**
     * Generates the Hitmap
     *
     * @private
     *
     * @param image Image
     * @returns {Uint32Array}
     */
    private generateHitMap(image: HTMLImageElement): Uint32Array {
        const canvas = document.createElement('canvas');

        canvas.width = image.width;
        canvas.height = image.height;

        const context = canvas.getContext('2d');

        if (context == null) {
            throw new Error('Invalid context 2d');
        }

        const threshold = 25;

        const w = canvas.width;
        const h = canvas.height;
        context.drawImage(image, 0, 0);

        const imageData = context.getImageData(0, 0, w, h);

        const hitmap = new Uint32Array(Math.ceil((w * h) / 32));
        for (let i = 0; i < w * h; i++) {
          const ind1 = i % 32;
          // eslint-disable-next-line no-bitwise
          const ind2 = (i / 32) | 0;
          if (imageData.data[i * 4 + 3] >= threshold) {
            // eslint-disable-next-line no-bitwise
            hitmap[ind2] = hitmap[ind2] | (1 << ind1);
          }
        }

        return hitmap;
    }
}