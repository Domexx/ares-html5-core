import {HitEvent} from '../Interface/HitEvent';

/**
 * @type HitEventHandler
 */
export type HitEventHandler = (event: HitEvent) => void;