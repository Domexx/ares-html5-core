/**
 * @type HitEventType
 */
export type HitEventType = 'click';