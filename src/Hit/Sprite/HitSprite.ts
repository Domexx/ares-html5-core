import {Sprite} from 'pixi.js';
import {HitDetectionElement} from '../Interface/HitDetectionElement';
import {HitDetectionNode} from '../Interface/HitDetectionNode';
import {HitEventHandler} from '../Type/HitEventHandler';
import {HitEventType} from '../Type/HitEventType';
import {HitTexture} from '../Texture/HitTexture';
import {HitDetectionRegister} from '../Interface/HitDetectionRegister';
import {HitEvent} from '../Interface/HitEvent';
import {Rect} from '../Interface/Rect';
import {Hitmap} from '../../Furniture/Type/Hitmap';

/**
 * @class HitSprite
 * @extends Sprite
 * @implements HitDetectionElement
 */
export class HitSprite extends Sprite implements HitDetectionElement {
    /**
     * @private
     * @property
     */
    private hitDetectionNode: HitDetectionNode | undefined;

    /**
     * @private
     * @property
     */
    private handlers = new Map<HitEventType, Set<HitEventHandler>>();

    /**
     * @private
     * @property
     */
    private hitTexture: HitTexture | undefined;

    /**
     * @private
     * @property
     */
    private tag: string | undefined;

    /**
     * @private
     * @property
     */
    private mirrored: boolean;

    /**
     * @private
     * @property
     */
    private mirrorNotVisually: boolean;

    /**
     * @private
     * @property
     */
    private ignore = false;

    /**
     * @private
     * @property
     */
    private getHitmap:
    | (() => (
        x: number,
        y: number,
        transform: { x: number; y: number }
      ) => boolean)
    | undefined;

    /**
     * @constructor
     */
    constructor({
        hitDetection,
        mirrored = false,
        mirroredNotVisually = false,
        getHitmap,
        tag,
    }: {
        hitDetection: HitDetectionRegister;
        getHitmap?: () => Hitmap;
        mirrored?: boolean;
        mirroredNotVisually?: boolean;
        tag?: string;
    }) {
        super();

        this.mirrored = mirrored;
        this.mirrorNotVisually = mirroredNotVisually;
        this.getHitmap = getHitmap;
        this.tag = tag;
        this.hitDetectionNode = hitDetection.register(this);
        this.mirrored = this.mirrored;
    }

    public trigger(type: HitEventType, event: HitEvent): void {
        const handlers = this.handlers.get(type);

        handlers?.forEach((handler) => handler({ ...event, tag: this.tag }));
    }

    public addEventListener(type: HitEventType, handler: HitEventHandler): void {
        const existingHandlers = this.handlers.get(type) ?? new Set();
        existingHandlers.add(handler);

        this.handlers.set(type, existingHandlers);
    }

    public removeEventListener(type: HitEventType, handler: HitEventHandler): void {
        const existingHandlers = this.handlers.get(type);
        existingHandlers?.delete(handler);
    }

    public destroy(): void {
        super.destroy();

        this.hitDetectionNode?.remove();
    }

    public hits(x: number, y: number): boolean {
        if (this.getHitmap == null || this.ignore) {
            return false;
        }

        const hits = this.getHitmap();

        return hits(x, y, { x: this.worldTransform.tx, y: this.worldTransform.ty });
    }

    public getHitBox(): Rect {
        if (this.mirrored || this.mirrorNotVisually) {
            return {
                x: this.worldTransform.tx - this.texture.width,
                y: this.worldTransform.ty,
                width: this.texture.width,
                height: this.texture.height,
                zIndex: this.zIndex
            };
        }

        return {
            x: this.worldTransform.tx,
            y: this.worldTransform.ty,
            width: this.texture.width,
            height: this.texture.height,
            zIndex: this.zIndex,
        };
    }

    public get spriteHitTexture(): HitTexture | undefined {
        return this.hitTexture;
    }

    public set spriteHitTexture(value) {
        if (value != null) {
            // @TODO set property value bya avatarloaderesult
            this.texture = value.texture;
            this.getHitmap = () => (
              x: number,
              y: number,
              transform: { x: number; y: number }
            ) =>
              value.hits(x, y, transform, {
                mirrorHorizonally: this._mirrored || this._mirrorNotVisually,
              });
          }
    }

    public get spriteIgnore(): boolean {
        return this.ignore;
    }

    public set spriteIgnore(value: boolean) {
        this.ignore = value;
    }

    public get spriteMirrored(): boolean {
        return this.mirrored;
    }

    public set spriteMirrored(value: boolean) {
        this.mirrored = value;
        this.scale.x = this.mirrored ? -1 : 1;
    }
}