import {HitDetectionElement} from './HitDetectionElement';
import {HitDetectionNode} from './HitDetectionNode';

/**
 * @interface HitDetectionRegister
 */
export interface HitDetectionRegister {
    register(rectangle: HitDetectionElement): HitDetectionNode;
}