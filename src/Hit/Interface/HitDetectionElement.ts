import {HitEventType} from '../Type/HitEventType';
import {HitEvent} from './HitEvent';
import {Rect} from './Rect';

/**
 * @interface HitDetectionElement
 */
export interface HitDetectionElement {
    getHitBox(): Rect;
    trigger(type: HitEventType, event: HitEvent): void;
    hits(x: number, y: number): boolean;
}