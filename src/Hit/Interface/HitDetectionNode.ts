/**
 * @interface HitDetectionNode
 */
export interface HitDetectionNode {
    remove(): void;
}