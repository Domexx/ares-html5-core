/**
 * @interface HitEvent
 */
export interface HitEvent {
    mouseEvent: MouseEvent;
    tag?: string;

    stopPropagation(): void;
    absorb(): void;
}