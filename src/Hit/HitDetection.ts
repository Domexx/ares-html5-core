import {HitDetectionRegister} from './Interface/HitDetectionRegister';
import {HitDetectionElement} from './Interface/HitDetectionElement';
import {Container, Graphics} from 'pixi.js';
import {Game} from '../Game';
import {HitDetectionNode} from './Interface/HitDetectionNode';
import {HitEvent} from './Interface/HitEvent';
import {injectable} from 'tsyringe';

@injectable()
/**
 * @class HitDetection
 * @implements HitDetectionRegister
 */
export class HitDetection implements HitDetectionRegister {
    /**
     * @private
     * @property
     */
    private counter = 0;

    /**
     * @private
     * @property
     */
    private map: Map<number, HitDetectionElement> = new Map();

    /**
     * @private
     * @property
     */
    private detectionContainer: Container | undefined;

    /**
     * @constructor
     * @param game Game
     */
    constructor(private game: Game) {
        game.application.view.addEventListener('click', (event) => this.handleClick(event), {
            capture: true
        });

        game.application.view.addEventListener('contextmenu', (event) => this.handleClick(event), {
            capture: true,
        });
    }

    /**
     * Registers the Detection
     *
     * @param rectangle Rectangle
     * @returns {HitDetectionNode}
     */
    public register(rectangle: HitDetectionElement): HitDetectionNode {
        const id = this.counter++;

        this.map.set(id, rectangle);

        return {
            remove: () => {
                this.map.delete(id);
            }
        };
    }

    /**
     * Handles the Mouse Click
     *
     * @param event MouseEvent
     * @returns {void}
     */
    public handleClick(event: MouseEvent): void {
        this.triggerEvent(
            event.clientX,
            event.clientY,
            // eslint-disable-next-line no-shadow
            (element, event) => element.trigger('click', event),
            event
        );
    }

    /**
     * Debugs the Hit Detection
     *
     * @returns {void}
     */
    private debugHitDetection(): void {
        if (this.detectionContainer !== undefined) {
            this.detectionContainer.destroy();
         }

         this.detectionContainer = new Container();

         this.map.forEach((value) => {
            const box = value.getHitBox();
            const graphics = new Graphics();

            graphics.x = box.x;
            graphics.y = box.y;
            graphics.beginFill(0x000000, 0.1);
            graphics.drawRect(0, 0, box.width, box.height);
            graphics.endFill();

            this.detectionContainer?.addChild(graphics);
         });

         this.game.application.stage.addChild(this.detectionContainer);
    }

    /**
     * Triggers the Event
     *
     * @param x X
     * @param y Y
     * @param invoke Invoke
     * @param domEvent DomEvent
     * @returns {void}
     */
    private triggerEvent(
        x: number,
        y: number,
        // eslint-disable-next-line no-shadow
        invoke: (element: HitDetectionElement, event: HitEvent) => void,
        domEvent: MouseEvent
    ): void {
        const elements = this.performHitTest(x, y);
        let stopped = false;

        const event: HitEvent = {
            stopPropagation: () => {
                stopped = true;
            },
            absorb: () => {
                domEvent?.stopImmediatePropagation();
            },
            mouseEvent: domEvent
        };

        // eslint-disable-next-line @typescript-eslint/prefer-for-of
        for (let i = 0; i < elements.length; i++) {
            if (stopped) {
                break;
            }
            const element = elements[i];

            invoke(element, event);
        }
    }

    /**
     * Performs the Hit Test
     *
     * @param x X
     * @param y Y
     */
    private performHitTest(x: number, y:number) {
        const rect = this.game.application.view.getBoundingClientRect();

        x = x - rect.x;
        y = y - rect.y;

        const entries = Array.from(this.map.values())
        .map((element) => ({
            hitBox: element.getHitBox(),
            element
        }));

        const ordered = entries.sort(
            (a, b) => b.hitBox.zIndex - a.hitBox.zIndex
        );

        const hit = ordered.filter(({ element, hitBox}) => {
            const inBoundsX = hitBox.x <= x && x <= hitBox.x + hitBox.width;
            const inBoundsY = hitBox.y <= y && y <= hitBox.y + hitBox.height;

            const inBounds = inBoundsX && inBoundsY;

            if (!inBounds) {
                return false;
            }

            return element.hits(x, y);
        });

        return hit.map(({ element }) => element);
    }
}