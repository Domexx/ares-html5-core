import {AnimationTicker} from '../Interface/AnimationTicker';
import {Game} from '../Game';
import {injectable} from 'tsyringe';

@injectable()
/**
 * @class Animation
 */
export class Animation implements AnimationTicker {
    /**
     * @private
     * @property
     */
    private frame = 0;

    /**
     * @private
     * @property
     */
    private counter = 0;

    /**
     * @private
     * @property
     */
    private animFps = 24;

    /**
     * @private
     * @property
     */
    private targetFps = 60;

    /**
     * @private
     * @property
     */
    private subscriptions = new Map<
        number,
        (frame: number, accurateFrame: number) => void
    >();

    /**
     * @constructor
     * @param game Game
     */
    constructor(game: Game) {
        game.application.ticker.maxFPS = this.targetFps;
        game.application.ticker.minFPS = this.targetFps;
        game.application.ticker.add(() => this.increment());
    }

    /**
     * Subscribes the Animation
     *
     * @param cb CB
     * @returns {void}
     */
    public subscribe(cb: (frame: number, accurateFrame: number) => void): () => void {
        const id = this.counter++;

        this.subscriptions.set(id, cb);
        return () => this.subscriptions.delete(id);
    }

    /**
     * Gets the current Animation
     *
     * @returns {number}
     */
    public current(): number {
        return this.getNormalizedFrame(this.frame).rounded;
    }

    /**
     * Gets the normalized Frame
     *
     * @param frame Frame
     */
    private getNormalizedFrame(frame: number) {
        const factor = this.animFps / this.targetFps;
        const calculatedFrame = frame * factor;

        return {
            rounded: Math.floor(calculatedFrame),
            pure: calculatedFrame
        };
    }

    /**
     * Increments the Frame
     *
     * @returns {void}
     */
    private increment(): void {
        this.frame += 1;
        const data = this.getNormalizedFrame(this.frame);

        this.subscriptions.forEach((cb) => cb(data.rounded, data.pure));
    }
}