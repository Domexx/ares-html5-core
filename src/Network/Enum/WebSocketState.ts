/**
 * @enum WebSocketState
 */
export enum WebSocketState {
    OPENED = 'open',
    CLOSED = 'close',
    ERROR = 'error',
    MESSAGE = 'message',
}
