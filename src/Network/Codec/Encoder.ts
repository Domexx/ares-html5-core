import ByteBuffer from 'bytebuffer';
import {singleton} from 'tsyringe';
import {Codec} from '../Interface/Codec';

@singleton()
/**
 * @class Encoder
 * @implements Codec
 */
export class Encoder implements Codec {
    public handle(buffer: ByteBuffer): ByteBuffer {
        buffer.writeInt(buffer.offset - 4, 0);
        return buffer.slice(0, buffer.offset);
    }
}
