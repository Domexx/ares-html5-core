import ByteBuffer from 'bytebuffer';
import {injectable, singleton} from 'tsyringe';
import {Codec} from '../Interface/Codec';
import {Connection} from '../Connection';
import {EventManager} from '../Communication/Event/Manager';
import {Event} from '../Communication/Event/Event';
import {Logger} from '../../Util/Logger';
import {Log} from '../../Decorator/Log';

@singleton()
/**
 * @class MessageDecoder
 * @implements Codec
 */
export class Decoder implements Codec {
    @Log()
    private logger: Logger;

    /**
     * MessageDecoder constructor
     *
     * @param eventManager
     */
    constructor(private eventManager: EventManager) {}

    /**
     * Decode incoming messages
     *
     * @param connection
     */
    handle(connection: Connection): void {
        if (!connection.dataBuffer || connection.dataBuffer.byteLength < 4) {
            return;
        }

        while (connection.dataBuffer.byteLength) {
            if (connection.dataBuffer.byteLength < 4) {
                break;
            }

            const container: ByteBuffer = ByteBuffer.wrap(
                connection.dataBuffer
            );
            const length: number = container.readInt32();

            if (length > connection.dataBuffer.byteLength - 4) {
                break;
            }

            const extracted: ByteBuffer = container.readBytes(length);

            const header: number = extracted.readShort();

            // Get event by header
            const event: Event | undefined = this.eventManager.get(header);

            if (event === undefined) {
                this.logger.log(`Could not find event with header ${header}`);
                return;
            }

            event.buffer = extracted;
            event.handle();

            this.logger.debug(
                `[${header}] ${event.constructor.name}: ${event.bodyString}`
            );

            connection.dataBuffer = connection.dataBuffer.slice(length + 4);
        }
    }
}
