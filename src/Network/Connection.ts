import '@abraham/reflection';
import {singleton} from 'tsyringe';
import {WebSocketState} from './Enum/WebSocketState';
import {ComposerManager} from './Communication/Composer/Manager';
import {Encoder} from './Codec/Encoder';
import {EventManager} from './Communication/Event/Manager';
import {Decoder} from './Codec/Decoder';
import {Composer} from './Communication/Composer/Composer';
import ByteBuffer from 'bytebuffer';

import './Communication/Composer/index';
import './Communication/Event/index';
import {Log} from '../Decorator/Log';
import {Logger} from '../Util/Logger';

@singleton()
/**
 * @class Connection
 */
export class Connection {
    @Log()
    private logger: Logger;

    /**
     * @private
     * @property
     */
    public dataBuffer: ArrayBuffer = new ArrayBuffer(0);

    /**
     * @private
     * @property
     */
    private socket: WebSocket;

    /**
     * @private
     * @property
     */
    private state$: WebSocketState = WebSocketState.CLOSED;

    /**
     * Connection constructor
     *
     * @param composerManager
     * @param encoder
     * @param decoder
     */
    constructor(
        private composerManager: ComposerManager,
        private encoder: Encoder,
        private decoder: Decoder
    ) {}

    /**
     * Initialize socket connection
     *
     * @param host
     */
    public init(host: string = 'ws://localhost:3000/?sso=penis1'): void {
        this.logger.log('Initializing websocket connection...');
        this.socket = new WebSocket(host);

        this.socket.addEventListener(WebSocketState.OPENED, (event: Event) =>
            this.onOpen(event)
        );
        this.socket.addEventListener(
            WebSocketState.CLOSED,
            this.onClose.bind(this)
        );
        this.socket.addEventListener(WebSocketState.ERROR, (event: Event) =>
            this.onError(event)
        );
        this.socket.addEventListener(
            WebSocketState.MESSAGE,
            (event: MessageEvent) => this.onMessage(event)
        );
    }

    /**
     * Send message to server
     *
     * @param composer
     */
    public send(composer: Composer | undefined): void {
        if (composer === undefined) {
            return;
        }

        // Flush data to buffer
        composer.compose();

        // Encode data
        const encodedComposer: ByteBuffer = this.encoder.handle(
            Reflect.get(composer.constructor, 'buffer')
        );

        this.logger.debug(
            `[${Number(Reflect.get(composer.constructor, 'header'))}] ${
                composer.constructor.name
            }: ${composer.bodyString}`
        );
        // Send data
        this.socket.send(encodedComposer.toBuffer());

        // We need to dispose the composer otherwise
        // it will create a chain of the old value
        // and new value in the buffer
        composer.dispose();
    }

    /**
     * Get connection state
     *
     * @public
     * @returns {WebSocketState}
     */
    public get state(): WebSocketState {
        return this.state$;
    }

    /**
     * Handle websocket open event
     *
     * @private
     * @param event
     */
    private onOpen(event: Event): void {
        this.state$ = WebSocketState.OPENED;
        this.send(this.composerManager.get(4000));
    }

    /**
     * Handle websocket close event
     *
     * @private
     * @param event
     */
    private onClose(event: CloseEvent): void {
        // Check if connection is already closed
        if (this.state$ === WebSocketState.CLOSED) {
            return;
        }

        console.log(event);
    }

    /**
     * Handle websocket error event
     *
     * @private
     * @param event
     */
    private onError(event: Event): void {
        console.error(event);
    }

    /**
     * Handle websocket message event
     *
     * @private
     * @param event
     */
    private onMessage(event: MessageEvent): void {
        if (!event) {
            return;
        }

        const reader = new FileReader();

        reader.readAsArrayBuffer(event.data);

        reader.onloadend = () => {
            this.dataBuffer = this.concatArrayBuffers(
                this.dataBuffer,
                reader.result as ArrayBuffer
            );

            if (!this.dataBuffer || !this.dataBuffer.byteLength) {
                return;
            }

            this.decoder.handle(this);
        };
    }

    /**
     * Chain buffers
     *
     * @param firstBuffer
     * @param secondBuffer
     * @returns {ArrayBuffer}
     */
    private concatArrayBuffers(
        firstBuffer: ArrayBuffer,
        secondBuffer: ArrayBuffer
    ): ArrayBuffer {
        const buffer = new Uint8Array(
            firstBuffer.byteLength + secondBuffer.byteLength
        );

        buffer.set(new Uint8Array(firstBuffer), 0);
        buffer.set(new Uint8Array(secondBuffer), firstBuffer.byteLength);

        return buffer.buffer;
    }
}
