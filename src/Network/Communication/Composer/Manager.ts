import {singleton} from 'tsyringe';
import {Log} from '../../../Decorator/Log';
import {Logger} from '../../../Util/Logger';
import {Composer} from './Composer';

@singleton()
export class ComposerManager {
    @Log()
    private logger: Logger;

    /**
     * @private
     * @property
     */
    private composers$: Map<number, Composer> = new Map<number, Composer>();

    /**
     * Add composer
     *
     * @public
     * @param header
     * @param compsoer
     */
    public add(header: number, composer: Composer): void {
        this.composers$.set(header, composer);
        this.logger.debug(
            `${composer.constructor.name} was added with header ${header}`
        );
    }

    /**
     * Get composer
     *
     * @param key
     * @returns {Composer | undefined}
     */
    public get(key: number): Composer | undefined {
        return this.composers.get(key);
    }

    /**
     * Get composer map
     *
     * @public
     * @returns {Map<number, Composer>}
     */
    public get composers(): Map<number, Composer> {
        return this.composers$;
    }
}
