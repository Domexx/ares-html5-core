import '@abraham/reflection';
import ByteBuffer from 'bytebuffer';
import {Log} from '../../../Decorator/Log';
import {Logger} from '../../../Util/Logger';

/**
 * @abstract Composer
 */
export abstract class Composer {
    @Log()
    private logger: Logger;

    /**
     * Dispose composer
     *
     * @public
     */
    public dispose(): void {
        const buffer = Reflect.get(this.constructor, 'buffer') as ByteBuffer;

        // Clear buffer
        buffer.clear();

        // Set header
        const header: number = Reflect.get(this.constructor, 'header');
        buffer.writeInt(0).writeShort(header);

        this.logger.debug(`[${header}] ${this.constructor.name} disposed`);
    }

    /**
     * Get buffer content as string
     *
     * @public
     * @returns {string}
     */
    public get bodyString(): string {
        const buffer = Reflect.get(this.constructor, 'buffer') as ByteBuffer;
        return buffer.toBinary(0, buffer.offset).toString();
    }

    /**
     * Write byte
     *
     * @protected
     * @param value
     * @param offset
     */
    protected writeByte(value: number, offset?: number): void {
        (Reflect.get(this.constructor, 'buffer') as ByteBuffer).writeByte(
            value,
            offset
        );
    }

    /**
     * Write short
     *
     * @protected
     * @param value
     */
    protected writeShort(value: number): void {
        (Reflect.get(this.constructor, 'buffer') as ByteBuffer).writeShort(
            value
        );
    }

    /**
     * Write number
     *
     * @protected
     * @param value
     */
    protected writeNumber(value: number): void {
        (Reflect.get(this.constructor, 'buffer') as ByteBuffer).writeInt(value);
    }

    /**
     * Write boolean
     *
     * @protected
     * @param value
     */
    protected writeBoolean(value: boolean): void {
        (Reflect.get(this.constructor, 'buffer') as ByteBuffer).writeByte(
            value ? 1 : 0
        );
    }

    /**
     * Write string
     *
     * @protected
     * @param value
     */
    protected writeString(value: string): void {
        if (!value) {
            (Reflect.get(this.constructor, 'buffer') as ByteBuffer).writeShort(
                0
            );
            return;
        }

        (Reflect.get(this.constructor, 'buffer') as ByteBuffer)
            .writeShort(ByteBuffer.calculateUTF8Bytes(value))
            .writeString(value);
    }

    abstract compose(): void;
}
