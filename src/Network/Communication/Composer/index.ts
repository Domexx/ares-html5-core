import {container} from 'tsyringe';
import {PongComposer} from './Client/Pong';
import {ReleaseVersion} from './Client/ReleaseVersion';

// Resolve composers to make them available
// in our DI container
container.resolve(ReleaseVersion);
container.resolve(PongComposer);
