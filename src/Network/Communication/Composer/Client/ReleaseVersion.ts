import {injectable} from 'tsyringe';
import {Outgoing} from '../../../../Decorator/Outgoing';
import {Composer} from '../Composer';
import {Header} from '../Header';

@injectable()
@Outgoing(Header.RELEASE_VERSION)
export class ReleaseVersion extends Composer {
    public compose(): void {
        this.writeString('ARES-HTML5-1.0.0.1');
    }
}
