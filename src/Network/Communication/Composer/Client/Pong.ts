import {injectable} from 'tsyringe';
import {Outgoing} from '../../../../Decorator/Outgoing';
import {Composer} from '../Composer';
import {Header} from '../Header';

@injectable()
@Outgoing(Header.CLIENT_PONG)
export class PongComposer extends Composer {
    public compose(): void {}
}
