import {container} from 'tsyringe';
import {PingEvent} from './Client/PingEvent';

// Resolve events to make them available
// in our DI container
container.resolve(PingEvent);
