import {singleton} from 'tsyringe';
import {Log} from '../../../Decorator/Log';
import {Logger} from '../../../Util/Logger';
import {Event} from './Event';

@singleton()
export class EventManager {
    @Log()
    private logger: Logger;

    /**
     * @private
     * @property
     */
    private events$: Map<number, Event> = new Map<number, Event>();

    /**
     * Add event
     *
     * @public
     * @param header
     * @param compsoer
     */
    public add(header: number, event: Event): void {
        this.events$.set(header, event);
        this.logger.debug(
            `${event.constructor.name} was added with header ${header}`
        );
    }

    /**
     * Get composer
     *
     * @param key
     * @returns {Event | undefined}
     */
    public get(key: number): Event | undefined {
        return this.events.get(key);
    }

    /**
     * Get event map
     *
     * @public
     * @returns {Map<number, Event>}
     */
    public get events(): Map<number, Event> {
        return this.events$;
    }
}
