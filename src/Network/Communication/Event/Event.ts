import {container} from 'tsyringe';
import {Connection} from '../../Connection';
import {Composer} from '../Composer/Composer';
import {ComposerManager} from '../Composer/Manager';

/**
 * @abstract Event
 */
export abstract class Event {
    /**
     * @property
     * @public
     */
    public buffer: ByteBuffer;

    /**
     * @property
     * @public
     */
    public length: number;

    /**
     * Read byte from buffer
     *
     * @returns {number}
     */
    public readByte(): number {
        if (!this.buffer) {
            return -1;
        }

        return this.buffer.readByte();
    }

    /**
     * Read bytes from buffer
     *
     * @param length
     * @returns {ByteBuffer}
     */
    public readBytes(length: number): ByteBuffer {
        return this.buffer.readBytes(length);
    }

    /**
     * Read boolean from buffer
     *
     * @returns {boolean}
     */
    public readBoolean(): boolean {
        return this.readByte() === 1;
    }

    /**
     * Read short from buffer
     *
     * @returns {number}
     */
    public readShort(): number {
        if (!this.buffer) {
            return -1;
        }

        return this.buffer.readInt16();
    }

    /**
     * Read integer from buffer
     *
     * @returns {number}
     */
    public readInt(): number {
        if (!this.buffer) {
            return -1;
        }

        return this.buffer.readInt32();
    }

    /**
     * Read string from buffer
     *
     * @returns {string}
     */
    public readString(): string {
        const buffer = this.buffer.readBytes(this.readShort());
        return buffer.toString('utf8');
    }

    /**
     * Get buffer content as stirng
     *
     * @public
     * @returns {string}
     */
    public get bodyString(): string {
        const buffer = this.buffer;
        return buffer.toBinary(0, buffer.offset).toString();
    }

    /**
     * Send data to server
     *
     * @protected
     * @param composer
     */
    protected send(composer: Composer | undefined): void {
        const connection: Connection = container.resolve(Connection);
        connection.send(composer);
    }

    /**
     * Get composer manager
     *
     * @protected
     * @returns {ComposerManager}
     */
    public get composerManager(): ComposerManager {
        return container.resolve(ComposerManager);
    }

    abstract handle(): void;
}
