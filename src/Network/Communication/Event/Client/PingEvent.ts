import {injectable} from 'tsyringe';
import {Incoming} from '../../../../Decorator/Incoming';
import {Event} from '../Event';
import {Header} from '../Header';
import {Header as ComposerHeader} from '../../Composer/Header';

@injectable()
@Incoming(Header.CLIENT_PING)
export class PingEvent extends Event {
    /**
     * Handle incoming ping message
     */
    handle(): void {
        this.send(this.composerManager.get(ComposerHeader.CLIENT_PONG));
    }
}
