export interface Codec {
    handle(...args: any[]): any;
}
