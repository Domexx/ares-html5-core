export interface Event {
    header: number;
    bytesAvailable: boolean;

    readByte(): number;
    readBytes(length: number): ByteBuffer;
    readBoolean(): boolean;
    readShort(): number;
    readInt(): number;
    readString(): string;
}
