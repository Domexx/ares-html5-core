/* eslint-disable @typescript-eslint/restrict-template-expressions */
import '@abraham/reflection';
import ByteBuffer from 'bytebuffer';
import {container} from 'tsyringe';
import {Header} from '../Network/Communication/Composer/Header';
import {ComposerManager} from '../Network/Communication/Composer/Manager';

/**
 * Define composer
 *
 * @param targetClass
 * @returns ClassDecorator
 */
export const Outgoing = (header: number | Header): ClassDecorator => {
    return (target) => {
        if (Number.isNaN(header) || !header) {
            throw new Error(`${target.constructor.name} header is missing`);
        }

        // Delete property
        if (!Reflect.deleteProperty(target, 'header')) {
            throw new Error(
                `Could not delete property "header" in class ${
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
                    target.constructor.name
                }`
            );
        }

        const options: PropertyDescriptor = {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-return
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            value: header,
        };

        // Define property
        if (!Reflect.defineProperty(target, 'header', options)) {
            throw new Error(
                `Could not define "header" property in class ${target.constructor.name}`
            );
        }

        const buffer = new ByteBuffer();

        // Set header
        buffer.writeInt(0).writeShort(header);

        // Define buffer proeprty
        if (!Reflect.defineProperty(target, 'buffer', {value: buffer})) {
            throw new Error(
                `Could not define "buffer" property in class ${target.constructor.name}`
            );
        }

        const composerManager: ComposerManager = container.resolve(
            ComposerManager
        );

        // Add composer
        composerManager.add(header, target.prototype);
    };
};
