/* eslint-disable @typescript-eslint/restrict-template-expressions */
import '@abraham/reflection';
import {container} from 'tsyringe';
import {Header} from '../Network/Communication/Event/Header';
import {EventManager} from '../Network/Communication/Event/Manager';

/**
 * Define incoming
 *
 * @param targetClass
 * @returns ClassDecorator
 */
export const Incoming = (header: number | Header): ClassDecorator => {
    return (target) => {
        if (Number.isNaN(header) || !header) {
            throw new Error(`${target.constructor.name} header is missing`);
        }

        // Delete property
        if (!Reflect.deleteProperty(target, 'header')) {
            throw new Error(
                `Could not delete property "header" in class ${
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
                    target.constructor.name
                }`
            );
        }

        const options: PropertyDescriptor = {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-return
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            value: header,
        };

        // Define property
        if (!Reflect.defineProperty(target, 'header', options)) {
            throw new Error(
                `Could not define "header" property in class ${target.constructor.name}`
            );
        }

        const eventManager: EventManager = container.resolve(EventManager);

        // Add composer
        eventManager.add(header, target.prototype);
    };
};
