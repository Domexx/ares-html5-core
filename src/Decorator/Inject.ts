/* eslint-disable @typescript-eslint/restrict-template-expressions */
import '@abraham/reflection';
import {container, InjectionToken} from 'tsyringe';

/**
 * Inject dependency through property
 *
 * @param targetClass
 * @returns PropertyDecorator
 */
export const Inject = (targetClass: InjectionToken): PropertyDecorator => {
    return (
        target: any,
        propertyKey: string | symbol
    ): PropertyDescriptor | undefined => {
        if (targetClass === undefined) {
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            throw new Error(
                `Failed to inject class into ${target.constructor.name} because target class is undefined`
            );
        }

        if (!Reflect.deleteProperty(target, propertyKey)) {
            throw new Error(
                `Could not delete property ${String(propertyKey)} in class ${
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
                    target.constructor.name
                }`
            );
        }

        const options: PropertyDescriptor = {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-return
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            value: container.resolve(targetClass),
        };

        if (!Reflect.defineProperty(target, propertyKey, options)) {
            throw new Error(
                `Could not define ${String(
                    propertyKey
                )} property in class ${targetClass.toString()}`
            );
        }

        return Reflect.getOwnPropertyDescriptor(target, propertyKey);
    };
};
