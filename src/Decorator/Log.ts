/* eslint-disable @typescript-eslint/restrict-template-expressions */
import '@abraham/reflection';
import {container} from 'tsyringe';
import {Logger} from '../Util/Logger';

/**
 * Inject dependency through property
 *
 * @returns CLassDecorator
 */
export const Log = (): PropertyDecorator => {
    return (
        target,
        propertyKey: string | symbol
    ): PropertyDescriptor | undefined => {
        // Resolve logger
        const logger = container.resolve(Logger);

        // Set class name
        logger.name = target.constructor.name;

        // Set property value
        Reflect.set(target, propertyKey, logger);

        return Reflect.getOwnPropertyDescriptor(target, propertyKey);
    };
};
