import {singleton} from 'tsyringe';
import {Application, SCALE_MODES, Texture, utils} from 'pixi.js';
import {Connection} from './Network/Connection';
import {Logger} from './Util/Logger';
import {Log} from './Decorator/Log';

@singleton()
/**
 * @class Game
 * @see https://github.com/jankuss/shroom/blob/master/src/objects/Shroom.ts
 */
export class Game {
    @Log()
    private logger: Logger;

    /**
     * @property
     * @private
     */
    public application$: Application;

    /**
     * @property
     * @private
     */
    private canvasElement: HTMLCanvasElement;

    /**
     * Game constructor
     */
    constructor(private connection: Connection) {
        // Load Ares logo
        this.loadAresOutput();

        // Skip PIXI console output
        utils.skipHello();

        this.logger.log('Starting Ares client');
        // Create new canvas element
        this.canvasElement = document.querySelector(
            '#game'
        ) as HTMLCanvasElement;

        // Create new PIXI application
        this.application$ = new Application({
            height: window.innerHeight,
            width: window.innerWidth,
            resizeTo: window,
            view: this.canvasElement,
            backgroundColor: 0x000000,
        });

        // Initialize connection
        this.connection.init();
    }

    /**
     * Load textures asynchronously
     *
     * @param path
     * @returns {Promise<Texture>}
     */
    public async loadTexture(path: string): Promise<Texture> {
        const image = new Image();

        image.crossOrigin = 'anonymous';
        image.src = `resources/${path}`;

        await new Promise<void>((resolve, reject) => {
            image.onload = () => {
                this.logger.debug(`Loaded texture "${path}"`);
                resolve();
            };

            image.onerror = () => {
                this.logger.error(`Could not load texture '${path}'`);
                reject(`Could not load texture '${path}'`);
            };
        });

        const texture: Texture = Texture.from(image);
        texture.baseTexture.scaleMode = SCALE_MODES.NEAREST;

        return texture;
    }

    /**
     * @public
     * @returns {Application}
     */
    public get application(): Application {
        return this.application$;
    }

    /**
     * Load and log the Ares logo
     *
     * @private
     */
    private loadAresOutput(): void {
        const image = new Image();

        image.onload = () => {
            const style = `
            font-size: 1.2rem;
            padding: 3.5rem 3.5rem 3.5rem 10rem;
            background-size: contain;
            background: url('http://localhost:8080/images/logo.png');
            background-repeat: no-repeat;
          `;

            console.log('%c ', style);
        };

        image.src = 'http://localhost:8080/images/logo.png';
    }
}
