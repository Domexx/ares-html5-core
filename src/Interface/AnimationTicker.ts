/**
 * @interface AnimationTicker
 */
export interface AnimationTicker {
  subscribe(cb: (
      frame: number,
       accurateFrame: number
    ) => void): () => void;

  current(): number;
}