/**
 * @interface Dimension
 */
export interface Dimension {
    height: number;
    width: number;
}
