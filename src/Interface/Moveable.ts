/**
 * @interface Moveable
 */
export interface Moveable {
    move(
        roomX: number,
        roomY: number,
        roomZ: number
    ): void;

    clearMovement(): void;
}