/**
 * @interface ScreenPositioned
 */
export interface ScreenPositioned {
    screenPosition: {
        x: number;
        y: number;
    } | undefined;
}