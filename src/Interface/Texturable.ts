import {Texture} from 'pixi.js';

export interface Texturable {
    texture: Texture | undefined;
    color: string | undefined;
}
